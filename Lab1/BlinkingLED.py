'''!
@file BlinkingLED.py
@brief Creates various blinking patterns of LED on Nucleo-L476RG
@details The program uses a button located on the Nucleo to interact with the
         user through a PuTTY interface. On startup, the program will welcome 
         the user with instructions on how to operate the program. By pressing
         the button, the user can then cycle between three different waveforms;
         Square Wave, Sine Wave, and Saw Wave. Once the button is pressed, the
         wave type will display on the PuTTY interface. The waves will cycle 
         infinitely until the program is terminated using Ctrl+C.
         
         @image html StateTransitionDiagram.png
         
         See code repository for this lab:
         https://bitbucket.org/rdeanr/me305/src/master/Lab1/   
         See Short Demo Video Here:
         https://vimeo.com/user164074360/review/668107171/277bbe74f7
         
@author Ryan Dean
@author Zachary Hendrix
@date 1/17/2022
'''

import time     #Importing 'time' module
import math     #Importing 'math' module
import pyb      #Importing 'pyb' module

def onButtonPressCallback(IRQ_src):
    '''! 
    @brief Callback function when button is pressed
    @details From user pressing the button, changing state of buttonPressed
             variable
    @param IRQ_src This is a reference the Nucleo that caused the interrupt to 
                   occur
    @return No value is returned in a callback function since this was called
            by the MicroPython interrupt system
    '''
    ## @brief Advances system after button pressed
    #  @details Holds True/False value for whether the button has been pressed 
    #           (True) or not (False). Used in later functions to advance the 
    #           state of the system into different waveforms. After value
    #           advances state, value is reset to False waiting for user input.
        
    global buttonPressed #Defines 'buttonPressed' variable as a global variable
    buttonPressed = True

def SquareWave(t):
    '''! 
    @brief Outputs square wave pattern
    @details Continuously receives value of miliseconds passed in Nulceo and
             compares it to a fixed reference time determined at instant of
             button press. These times are compared and instantaneously change 
             the duty cycle of the LED using pulse width modulation from 0% to 
             100% every second. Function only continues when 1 second has 
             passed. 
    @param t Current instant in time in miliseconds
    @return Duty cycle percentage of LED
    '''
    ## @brief Flips output of square wave
    #  @details Used to count every time SquareWave(t) is successfully called
    #   Alernation between an even and odd count will change duty cycle of LED
    global g       #Defines 'g' variable as a global variable
    
    ## @brief Instant of time when button pressed.
    #  @details A value of time determined at the moment the button is pressed. 
    #           Used to as a reference value to determine appropriate timing of
    #           LED duty cycle patterns
    global now     #Defines 'now' variable as a global variable

    ## @brief Contains duty cycle percentage of LED for square wave pattern
    #  @details Strictly used in SquareWave(t). It is returned as the duty 
    #           cycle percentage of the LED. Only alternates between 0% and 
    #           100% and upon button press, the LED brightness will 100%
    
    global A #Defines 'A' variable as a global variable
    
    if time.ticks_diff(t, now) < 500 and g == 0:
        A = 100
    
    elif time.ticks_diff(t, now) >= 500:
        now = time.ticks_ms()
        g += 1
        if g%2 == 1:
            A = 0    
        elif g%2 == 0:
            A = 100
    else:
        pass
    return A

def SineWave(t):
    '''! 
    @brief Outputs sine wave pattern
    @details Continuously receives value of miliseconds passed in Nulceo and
             is subtracted by a fixed reference time determined at instant of
             button press. The output of this substraction is converted into
             rad/s and passed into a sinwave function that osciallates between
             0 and 1. This is multiplied by 100 to get a duty cycle percentage.
    @param t Current instant in time in miliseconds
    @return Duty cycle percentage of LED
    '''
    ## @brief Contains duty cycle percentage of LED for sine wave pattern
    #  @details Strictly used in SineWave(t). It is returned as the duty cycle
    #           percentage of the LED. Only oscillated between 0% and 100%
    #           continuously. Upon button press, the LED brightness will be 50%
    global B
    
    B = (.5 * math.sin(math.pi / 5 *(t - now)/1000) + .5)*100
    return B

def SawWave(t):
    '''! 
    @brief Outputs saw wave pattern
    @details Continuously receives value of miliseconds passed in Nulceo and
             is subtracted by a fixed reference time determined at instant of
             button press. Modulo division is then performed by the required 
             time interval. This is converted into the appropriate timing. 
             The output results is a linear increase in duty cycle percentage 
             until 100% duty cycle is reached. The duty cycle then resets and 
             linearlly increases again over and over at the appropriate 
             frequency.
    @param t Current instant in time in miliseconds
    @return Duty cycle percentage of LED
    '''
    ## @brief Contains duty cycle percentage of LED for saw wave pattern
    #  @details Strictly used in SawWave(t). It is returned as the duty cycle
    #           percentage of the LED. Only linearly increases 0% and 100%
    #           continuously. Upon button press, the LED brightness will 0%.
    global C

    C = ((t - now)%1000)/10
    return C


if __name__ == '__main__':
    buttonPressed = False
    
    ## @brief Variable used to identify which LED pattern program currently in.
    state = 0
    
    ## @brief pyb.Pin object corresponding to pin number A5.  
    pinA5 = pyb.Pin(pyb.Pin.cpu.A5)
    
    ## @brief pyb.Timer class setting LED blinking frequency.
    #  @details Sets the LED blinking frequency to 20000Hz.
    tim2 = pyb.Timer(2, freq = 20000)
    
    ## @brief pyb.Timer channel object created.
    #  @details Configures channel to Pulse Width Modulation mode on pinA5.
    t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)
    
    ## @brief pyb.Pin object corresponding to pin number C13.
    pinC13 = pyb.Pin(pyb.Pin.cpu.C13)
    
    ## @brief Object that assoociates callback function to pin with external 
    #         interrupt. 
    ButtonInt = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_FALLING, 
                           pull=pyb.Pin.PULL_NONE, 
                           callback = onButtonPressCallback)
    g = 0
    now = time.ticks_ms()
    
    print('Welcome to the program, here you can interact with the Nucleo LED \
(LD2) by cycling through LED blinking patterns. You can cycle through Square,\
Sine, and Saw Wave patterns by pressing B1 on the Nucleo (blue button). \
After pressing the button, the wave pattern \will be indicated on the PuTTY \
interface. Use Ctrl+C to exit the \program at any time.')
    
    
# The following code will continue indefinitely until there is a Keyboard 
# interrupt from the user. Each time the button is pressed, the value of
# buttonPressed will be reset.     
    while True:
        try:
            
            if state == 0: #Run State 0
                t2ch1.pulse_width_percent(0) # LED kept off
                
                if buttonPressed:
                    buttonPressed = False    # Resets state of button
                    state = 1
                    now = time.ticks_ms()    # Redefines reference time
                    print ('Square Wave Selected')
            elif state == 1: # Run State 1
                brt = SquareWave(time.ticks_ms())
                t2ch1.pulse_width_percent(brt) # LED continusouly updated until
                                               # state change
                
                if buttonPressed:
                    buttonPressed = False    # Resets state of button
                    state = 2
                    g = 0
                    now = time.ticks_ms()    # Redefines reference time
                    print('Sine Wave Selected')
            elif state == 2: #Run State 2
                brt = SineWave(time.ticks_ms())
                t2ch1.pulse_width_percent(brt) # LED continusouly updated until
                                               # state change
                
                if buttonPressed:
                    buttonPressed = False    # Resets state of button
                    state = 3
                    now = time.ticks_ms()    # Redefines reference time
                    print('Saw Wave Selected')
            elif state == 3:
                brt = SawWave(time.ticks_ms())
                t2ch1.pulse_width_percent(brt) # LED continusouly updated until
                                               # state change
                
                if buttonPressed:
                    buttonPressed = False    # Resets state of button
                    state = 0
                    now = time.ticks_ms()    # Redefines reference time
                    print('You have reached the end of the available cycles.\
Keep pressing the button to continue cycling or use Ctrl+C to exit the program.')
            
        except KeyboardInterrupt:
            break
    print('Program Terminating')