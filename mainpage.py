'''!@file                mainpage.py
    @brief               Brief doc for mainpage.py
    @details             Detailed doc for mainpage.py 

    @mainpage

    @section sec_intro   Introduction
                        This website documents the code required to balance a
                        ball on a platform autonomously. Use the top pull down
                        tabs to navigate to all classes and files used to
                        accomplish this. Each file is organized under the lab
                        assignment where it was performed. All labs teach
                        valuable elements of mechatronics that culminate into
                        a the final project. See the image below for a general
                        setup of the ball balancing platform.
    @image              html Platform.jpg
    
                        For access to the entire code repository, click on the
                        link below. Navigating to the main.py file of each lab
                        section will also provide a direct link. 
                        https://bitbucket.org/rdeanr/me305/src/master/  
    @section sec_mod     Balancing Platform Analysis and Modeling
                        The behavior of a ball on a platform was modeled in 
                        MATLAB and can be found in the
    @ref                Balancing1
                        and
    @ref                Balancing2
                        pages.

    @section sec_term    Ball Balancing Platform
                        See
    @ref                TP
                        for in-depth details regarding the term project and its
                        implementation.
    @section sec_drive   Drivers Used
                        The information below details the driver files used to
                        run the program and build a foundational knowledge of
                        mechatronics.
     @section sec_mot    Motor Driver
                        Please see motor.Motor for details. This has the
                         capability to change the speed of each motor.
     @section sec_enc    Encoder Driver
                        Please see encoder.Encoder for details. It has the
                        capability to return encoder position, speed, and zero
                        the encoder.
     @section sec_adc    ADC Driver
                        Please see ADC_driver.ADC_Driver for details. This has
                        the capability to read touch sensor data and calculate
                        calibration coefficients as well as read/write the
                        coefficients from/to a text file.
     @section sec_bno    IMU Driver
                        Please see BNO_driver.BNO055 for details. This has the
                        capability to find the orientation and angular velocity
                        of the ball balancing platform. It also finds the 
                        calibration status of the IMU and read/write
                        calibration coefficients from/to a text file.    
    @section sec_lab     Labs
                        This section details all labs critical to developing
                        the ball balancing platform.
    @section sec_l3m     Lab 3
                        This lab looks at how to control DC motors through
                        pulse width modulation by creating task files that
                        can directly interface with the motor and encoder. A
                        variety of commands are implemented such as finding
                        the encoder position, velocity, setting the motor duty
                        cycle, collecting data, and setting up a testing
                        interface. You can find more information here:
    @ref                lab3
    @section sec_l4m     Lab 4
                        This lab looks at the implementation of closed loop
                        speed control of a permanent magnet DC motor. You can
                        find more information here:
    @ref                lab4
    
    @section sec_l5m     Lab 5
                        This lab looks at the implementation of closed loop
                        control to automatically level a platform, even after
                        tapping. You can find more information here:
    @ref                lab5
    

    @author             Ryan Dean
    @author             Zachary Hendrix

    @date               February 3, 2022
'''