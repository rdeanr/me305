# -*- coding: utf-8 -*-
"""
Created on Thu Jan  6 08:29:44 2022

@author: Ryan Dean
"""

def fib(idx):
    '''
    @brief     This function calculates a Fibonacci number at a specific index.
    @param idx An integer specifying the index of the desired
    Fibonacci number
    '''
    if idx == 0:
        output = 0
    elif idx == 1:
        output = 1
    elif idx > 1:
        f = [0,1]               # Creates a base list of the first Fibonacci numbers
        n = 0
        while n < idx:
            add = f[n+1] + f[n]
            f.append(add)       # Adds new Fibonacci number to end of list
            n += 1              # with each loop
        output = f[idx]
        
    return output
    
if __name__ == '__main__':  # Below code will only run when the main file is run
    
    # This is the user interface. It is programmed to only accept positive 
    # integers using if, try, and except statements. It will loop when
    # an error is encountered when trying to convert to an integer
    # and loop when the integer is not postive.
    
    while True:
        try:
            idx = int(input('Choose an index:'))
            if idx < 0:
                print('Please input a positive integer.')
                continue
            elif idx >= 0:
                out = fib(idx)
                print(f'The Fibonacci number at index {idx} is {out}.')
                state = input('Enter "q" to quit or press any key to continue:')
                if state == 'q':
                    break
                else:
                    continue
                
        except ValueError:
            print('Please input a positive integer.')
  
