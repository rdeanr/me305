'''!@file   Balancing_Platform_Model.py
    @brief  MATLAB modeling of balancing platform project
    
    @page Balancing2 Platform_Modeling
    
    The images below show the modeling required to determine the position
    of the ball and platform as a function of time.
      
    @image html Page1.png
    @image html Page2.png
    @image html Page3.png
    @image html Page4.png
    @image html Page5.png

'''    