'''!
@file Balancing_Platform_Analysis.py
@brief Dynamic analysis for motion of a ball
@details Analyzes the motion of a ball with the tilt of a platform. Results in
         a matrix that will be used to relate motor input to platform motion.
         
         The below images show the analysis for this motion:
         @image html Balance_Analysis_1.png
         @image html Balance_Analysis_2.png
         @image html Balance_Analysis_3.png
         @image html Balance_Analysis_4.png
         @image html Balance_Analysis_5.png
         
@author Ryan Dean

@date 2/1/2022
'''