'''!
@file task_user.py
@brief Handles the user UI of our encoder system
@details This file interacts directly with user keyboard inputs to then send
         the request to the task_encoder.py file. It uses six states that
         correspond to different key in puts to send the program into its
         appropriate settings based on the user input. These states are
         highlighted below:
         
         S0-Initialization
         S1-Command
         S2-Zero
         S3-Position
         S4-Delta
         S5-Data
         S6-Help
         
         The below image shows the state transition diagram employed for this
         task file:
         @image html Task_User_State_Diagram.png

@author Ryan Dean
@author Zachary Hendrix
@date 2/1/2022
'''

from time import ticks_diff, ticks_add, ticks_us, ticks_ms
from pyb import USB_VCP
from shares import Share
import micropython
from task_encoder import taskEncoder
import encoder
import array as arr

##@brief State 0 - Initialize
# @details State that corresponds to initializing encoder 1 and printing out
#          the initial help menu.
S0_INIT = micropython.const(0)

##@brief State 1 - Command
# @details State that waits for a user input and transfers the user to the
#          correct state based on that input.
S1_CMD  = micropython.const(1)

##@brief State 2 - Zero Encoder
# @details State that zeros out the encoder
S2_ZERO = micropython.const(2)

##@brief State 3 - Position
# @details State that returns position to the user
S3_POS  = micropython.const(3)

##@brief State 4 - Delta
# @details State that returns the delta(speed) of the encoder to the user
S4_DEL  = micropython.const(4)

##@brief State 5 - Data Collection
# @details State that collects 30 seconds of data and outputs it to the user
S5_DATA = micropython.const(5)

##@brief State 6 - Help
# @details State that returns a help window to the user
S6_HELP  = micropython.const(6)

def taskUser(taskname, period, zFlag, pFlag, dFlag, gFlag, position, delta,P,T):
    '''!@brief A generator to implement the UI task as an Finite State Machine.
        @details The task runs as a generator function and requires a task name
                 and interval to be specified. This task file receives flag
                 values altered by the user. These flags are then shared with
                 task_encoder.py where that task file can execute actions
                 defined in encoder.py. Once the desired task is completed,
                 the state is reset back to idle state (State 1).
        @param taskName The name of the task as a string.
        @param period Specifies how long the task has to perform its function
                      in microseconds. Specified as an integer.
        @param zFlag Holds True or False value to know when zeroing command was
               called by the user to zero the current position of the encoder.
        @param pFlag Holds True or False value to know when position command
                     was called by the user to know the current position of the
                     encoder.
        @param dFlag Holds True or False value to know when delta command was
                     called by the user to know the current delta (speed) of 
                     the encoder.
        @param gFlag Holds True or False value to know when data collection
                     command was called by the user to collect current time and
                     position data.
        @param position Stores current position of encoder to be read in 
                        task_user.py
        @param delta Stores the delta (speed) of the encoder to be read in
                     to task_user.py
        @param P Stores current position of encoder for data collection
        @param T Stores current time of encoder for data collection
        @return yield None            
    '''
    
    ##@brief State Variable
    # @details Stores the state we are in so the program knows which code to
    #          run
    state = S0_INIT
    print("+-----------------------------------------------------+")
    print("|          ME 305: Encoder Testing Interface          |")
    print("+-----------------------------------------------------+")
    print("| Use the following Commands:                         |")
    print("| z or Z | Zero the position of encoder 1             |")
    print("| p or P | Print out the position of encoder 1        |")
    print("| p or P | Print out the position of encoder 1        |")
    print("| d or D | Print the delta (speed) of encoder 1       |")
    print("| g or G | Collect data from encoder 1 for 30 seconds |")
    print("| s or S | End data collection early                  |")
    print("| h or H | Return to this help page                   |")
    print("| Ctrl-C | Terminate Program                          |")
    print("+-----------------------------------------------------+")
    
    # A timestamp, in microseconds, indicating when the next iteration of the
    # generator must run.
    
    ##@brief Start time
    # @details Records the time in microseconds the program started running in 
    #          order to calculate the time passed since start
    start_time = ticks_us()
    
    ##@brief Next Cycle time
    # @details State that returnsthe next time in the cycle
    next_time = ticks_add(start_time, period)
    
    ##@brief Initialize the serport to read data
    # @details A (virtual) serial port object used for getting characters 
    #          cooperatively.
    serport = USB_VCP()
    
    ##@brief Time array
    # @details Array used to store variables of time
    #          collected over 30 seconds (3000 indicies)
    DataP = arr.array('l', 3001*[0])
    
    ##@brief Position array
    # @details Array used to store variables of position
    #          collected over 30 seconds (3000 indicies)
    DataT = arr.array('l', 3001*[0])
    
    
    # The finite state machine must run indefinitely.
    while True:
        # We should only call the ticks_us() function once per iteration of the 
        # of the task to "timestamp" the task iteration. This value can be used 
        # internally within the states if timestamps are require for data
        # collection.
        
        ##@brief Finds current time of timer
        current_time = ticks_us()
    
        # The finite state machine only needs to run if the interval has elapsed
        # as indicated by current_time being greater than next_time. We can't
        # do direct comparison or arithmetic of ticks_us() values because of the
        # internal rollover/overflow of the ticks value. Therefore we have to
        # use ticks_diff() to find the difference in ticks value which then can 
        # be compared to zero to check which is greater.
        if ticks_diff(current_time,next_time) >= 0:
        
            # Once the task does need to run we must prepare for the next
            # iteration by adjusting the value of next_time by adding the task
            # interval. Again we can't use normal arithmetic so the ticks_add()
            # function is used.
            next_time = ticks_add(next_time, period)
            
            # State 0 code
            if state == S0_INIT:             
                state = S1_CMD # transition to state 1
            
                
            # State 1 code
            elif state == S1_CMD:
                
                if serport.any():
                # Read one character and decode it into a string
                    ##@brief Input Character
                    # @details Reads one character and decodes it into a string
                    #          . Then it stores the input character.
                    charIn = serport.read(1).decode()
                    
                    if charIn in {'z','Z'}:
                        print("Zeroing Encoder")
                        zFlag.write(True)
                        state = S2_ZERO
                    elif charIn in {'p', 'P'}:
                        print("Encoder Position")
                        pFlag.write(True)
                        state = S3_POS
                    elif charIn in {'d', 'D'}:
                        print("Delta Position")
                        dFlag.write(True)
                        state = S4_DEL
                    elif charIn in {'g', 'G'}:
                        print("-------------------Collecting Data---------------------")
                        gFlag.write(True)
                        
                                         
                        ##@brief Initial time of data collection
                        # @details Stores the inital time of the data
                        #          collection sequence to calculate relative
                        #          time from the start of the sequence
                        gstart_time = ticks_ms()
                        state = S5_DATA
                       
                        ##@brief Data Collection index
                        # @details Used to put data in the right place in the
                        #          DataP and DataT array
                        i = 0
                    elif charIn in {'h', 'H'}:
                        print("+-----------------------------------------------------+")
                        print("|          ME 305: Encoder Testing Interface          |")
                        print("+-----------------------------------------------------+")
                        print("| Use the following Commands:                         |")
                        print("| z or Z | Zero the position of encoder 1             |")
                        print("| p or P | Print out the position of encoder 1        |")
                        print("| d or D | Print the delta (speed) of encoder 1       |")
                        print("| g or G | Collect data from encoder 1 for 30 seconds |")
                        print("| s or S | End data collection early                  |")
                        print("| h or H | Return to this help page                   |")
                        print("| Ctrl-C | Terminate Program                          |")
                        print("+-----------------------------------------------------+")
                    else:
                        print(f"You typed {charIn} from state 1 at t={ticks_diff(current_time,start_time)/1e6}[s].")
                    
            
            elif state == S2_ZERO:
                if not zFlag.read():
                    print("Zeroing Completed")
                    state = S1_CMD
            elif state == S3_POS:
                if not pFlag.read():
                    print(position.read())
                    state = S1_CMD
            elif state == S4_DEL:
                if not dFlag.read():
                    print(delta.read())
                    state = S1_CMD
            elif state == S5_DATA:
                
                if serport.any():
                    charIn = serport.read(1).decode()
                    
                    if charIn in {'s', 'S'}:
                        gFlag.write(False)

                        for (a,b) in zip(DataT[:i],DataP[:i]):
                            print(f"{(a - gstart_time)/1000:.2f}, {b}")
                        print("---------------------------End Data--------------------------")
                        state = S1_CMD                        
                elif i >= 3001:
                    gFlag.write(False)
                    for (a,b) in zip(DataT,DataP):
                        print(f"{(a - gstart_time)/1000:.2f}, {b}")
                    print("---------------------------End Data--------------------------")                
                    state = S1_CMD
                                    
                else:
                    DataT[i] = T.read()
                    DataP[i] = P.read()
                    i += 1
                    
                    
            else:
                raise ValueError(f"Invalid state value in {taskName}: State {state} does not exist")
            
            # After a valid run of the state machine we can yield the state.
            # This yielded value could be stored in the main loop below to trace
            # the state transitions.
            yield None
        
        # If the time has not come yet to run the task we can just exit early by
        # yielding nothing.
        else:
            yield None


