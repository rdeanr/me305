'''!
@file main.py
@brief Initializes values and manages tasks
@details This is the main file to run the tasks required to interact with the 
         encoder. It will continually call the specified task until the user
         interrupts the program by ending it with Keyboard Interrupt (Ctrl-C).
         Variables shared between tasks are initialized here and the tasks, and
         there periods are defined to run at 100Hz. Pictures of the state
         transition diagrams can be found in their respective task files. A
         picture of the task diagram can be found below.
         To view the code repository for this lab, see link below.
         
         Code repository for this lab:
         https://bitbucket.org/rdeanr/me305/src/master/Lab2/
         
         @image html Encoder_Position_Plot.png
         
         The plot below shows the position output of the encoder over a period
         of 30 seconds using the data collection feature inherent to this
         program. The position was changed by manually rotating the encoder.
         
         @image html Task_Diagram.png
         
         
         
         

@author Ryan Dean
@author Zachary Hendrix
@date 2/3/2022
'''
import encoder
import shares
from time import ticks_diff, ticks_add, ticks_us
from pyb import USB_VCP
import micropython
from task_encoder import taskEncoder
from task_user import taskUser


## @brief Signal that the button z has been pressed
#  @details Shared object so we can update its value in every file. It turns true after z is pressed, and turns false
#          after the zeroing cycle has ended.
zFlag = shares.Share(False)

## @brief Signal that the button p has been pressed
#  @details Shared object so we can update its value in every file. It turns true after z is pressed, and turns false
#          after the position has been returned.
pFlag = shares.Share(False)

## @brief Signal that the button d has been pressed
#  @details Shared object so we can update its value in every file. It turns true after z is pressed, and turns false
#          after the delta has been returned.
dFlag = shares.Share(False)

## @brief Signal that the button g has been pressed
#  @details Shared object so we can update its value in every file. It turns true after g is pressed, and turns false
#          after the data collection cycle has ended.
gFlag = shares.Share(False)

## @brief Stores position of encoder for transfer between files.
#  @details Used to communicate between task_encoder and task_user in order to pass the position across those files.
position = shares.Share(0)

## @brief Stores delta (speed) of encoder
delta = shares.Share(0)

## @brief Stores a single data point reading of encoder position
P = shares.Share(0)

## @brief Stores a single data point reading of encoder time
T = shares.Share(0)


# A generator object created from the generator function for the user
# interface task.


## @brief Holds task information and settings to pass into task_user.py
task1 = taskUser('taskUser', 10_000, zFlag, pFlag, dFlag, gFlag, position, delta,P,T)

## @brief Holds task information and settings to pass into task_encoder.py
task2 = taskEncoder('taskEncoder',10_000, zFlag, pFlag, dFlag, gFlag, position, delta,P,T)

# Creating a task list that can be iterated through. Adding more task objects 
# to the list will allow the tasks to run together cooperatively.

## @brief List of all tasks in program
#  @details This list will be used to cycle through every task in the list.
taskList = [task1,task2]


# The system should run indefinitely until the user interrupts program flow
# by using a Ctrl-C to trigger a keyboard interrupt. The tasks are run by
# using the next() function on each item in the task list.
while True:
    try:
        for task in taskList:
            next(task)
    
    except KeyboardInterrupt:
        break
        
# In addition to exiting the program and informing the user any cleanup code
# should go here. An example might be un-configuring certain hardware
# peripherals like the User LED or perhaps any callback functions.
print('Program Terminating')

