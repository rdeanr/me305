'''!
@file BNO_driver.py
@brief Creates BNO055 driver class
@details This task file directly interacts with the IMU to read platform
         orientation and velocity data. It can also change the mode of the IMU
         and reads/writes calibration coefficient data from and to a text file.

@author Ryan Dean
@author Zachary Hendrix
@date 3/17/2022
'''

import pyb
from pyb import I2C
import struct
import time
from pyb import USB_VCP

serport = USB_VCP()

class BNO055:
    '''! 
    @brief BNO055 Class
    @details Changes the mode of the IMU and reads/writes calibration coefficient
             data from and to a text file.
             
    '''
    def __init__ (self):
        '''!
        @brief Initializes IMU settings
        @details Changes the mode of the IMU and reconfigures axes to correct
                 orientation for project
        @return None
        '''
        ## @brief Sets IMU to controller mode
        self.i2c = I2C(1, I2C.CONTROLLER)
        
        ## @brief Stores reference address of IMU
        self.addr = I2C.scan(self.i2c)
        
        ## @brief Object to set IMU to configuration mode
        self.i2c.mem_write(0b0000,self.addr[0],0x3D) # Sets Configuration Mode
        
        time.sleep(0.05)
        
        ## @brief Object to change default IMU axis configuration
        self.i2c.mem_write(0x21,self.addr[0],0x41)
        
        ## @brief Object to change default IMU axis signs
        self.i2c.mem_write(0x02,self.addr[0],0x42)
        
        ## @brief Object to change the IMU mode to 9 degrees of freedom (NDOF)
        self.i2c.mem_write(0b1100,self.addr[0],0x3D) # Sets to NDOF
        
    
    def op_mode(self,bit):
        '''!
        @brief Method to change the operating mode of IMU
        @param bit Contains register value to change operating mode of IMU
        @return None
        '''
        self.i2c.mem_write(bit,self.addr[0],0x3D)
        return None
    
    def calib_status(self):
        '''!
        @brief Reads calibration status of platform
        @details Reads accelerometer, gyroscopic, and magnetometer calibation
                 statuses
        @return List of hexadecimal values representing calibration status of 
                IMU
        '''
        
        ## @brief Calibration status of IMU
        #  @details Returns an 8 bit value containing calibration status of 
        #           the accelerometer, gyroscope, and magnetometer with 3 being
        #           the highest level of calibration
        self.data = self.i2c.mem_read(1,self.addr[0],0x35)[0]
        return self.data
        
    def calib_coeff_read(self):
        '''!
        @brief Reads calibration offsets of IMU and write to text file
        @details Calibration offsets contain values for accelerometer, 
                 gyroscope, and magnetometer. These are then written to a text
                 file.
        @return None
        '''
        
        ## @brief List of 8 bit calibration offset values in binary read from 
        #         IMU
        cal_bytes = self.i2c.mem_read(22,self.addr[0],0x55)
        
        str_list = []
        for cal_byte in cal_bytes:
            str_list.append(hex(cal_byte))
        
        y = ','.join(str_list)
        f = open('IMU_cal_coeffs.txt','w')
        f.write(y)
        f.close()
        
    
    
    def calib_coeff_write(self):
        '''!
        @brief Writes calibration offsets to IMU
        @details Reads a calibration text file and converts offsets into a
                 format to be ran into the IMU.
        @return None
        '''
        with open('IMU_cal_coeffs.txt', 'r') as f:
            
            ## @brief String of the first line of text file
            #  @details Contains calibration coefficient data in string form
            cal_data_string = f.readline()
            
            ## @brief Float values converted into demical form and stored as a
            #         list.
            cal_values = [int(cal_value,16) for cal_value in cal_data_string.strip().split(',')]
        i = 0
        for cal_value in cal_values:
            self.i2c.mem_write(cal_value,self.addr[0], 85 + i )
            i += 1
    
    def read_Euler(self):
        '''!
        @brief Reads platform Euler angles
        @details Reads platform Euler angles and converts most and least
                 significant bits into a single binary form. Then converts to 
                 decimal in degrees.
        @return None
        '''
        
        ## @brief List of 8 bit binary values containing Euler angle position 
        #         of platform
        self.buffE = self.i2c.mem_read(6,self.addr[0],0x1A,addr_size = 8)
        Euler_bit = struct.unpack('<hhh',self.buffE)
        Euler_degree = tuple(E/16 for E in Euler_bit)
        (Euler_x,Euler_y,Euler_z) = (Euler_degree[0],Euler_degree[1]+2,Euler_degree[2]) 
        return (Euler_x,Euler_y,Euler_z)
    
    def read_angvelo(self):
        '''!
        @brief Reads platform angular velocities
        @details Reads platform angular velocities and converts most and least
                 significant bits into a single binary form. Then converts to 
                 decimal in degrees/s.
        @return None
        '''
        
        ## @brief List of 8 bit binary values containing angular velocity of
        #         platform
        self.buffV = self.i2c.mem_read(6,self.addr[0],0x14,addr_size = 8)
        Angvelo_bit = struct.unpack('<hhh',self.buffV)
        Angvelo_degree = tuple(V/16 for V in Angvelo_bit)

        (Angvelo_x,Angvelo_y,Angvelo_z) = Angvelo_degree
        return (Angvelo_x,Angvelo_y,Angvelo_z)
        
        
        
    