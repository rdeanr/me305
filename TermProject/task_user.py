'''!
@file task_user.py
@brief Handles the user UI of our encoder system
@details This file interacts directly with user keyboard inputs to then send
         the request to the task_encoder.py file. It uses six states that
         correspond to different key in puts to send the program into its
         appropriate settings based on the user input. 

         See
@ref     TP 
         for state transition diagrams, task diagrams, plots and video
         demonstrating the balancing of the ball on the platform along with how
         the UI was implemented. 

@author Ryan Dean
@author Zachary Hendrix
@date 2/1/2022
'''

from time import ticks_diff, ticks_add, ticks_us, ticks_ms
from pyb import USB_VCP
from shares import Share
import micropython
import array as arr
import gc
import closedloop
import os
import ADC_driver
from ulab import numpy as np


## @brief State 0 - Initialize
#  @details State that corresponds to initializing encoder 1 and printing out
#          the initial help menu.
S0_INIT = micropython.const(0)

## @brief State 1 - Command
#  @details State that waits for a user input and transfers the user to the
#          correct state based on that input.
S1_CMD  = micropython.const(1)

## @brief State 2 - Changes mode of IMU
#  
S2_MODE = micropython.const(2)

## @brief State 3 - Position
#  @details State that returns position to the user
S3_POS  = micropython.const(3)

## @brief State 4 - Find Velocity
#  @details Calculates the velocity of motor 1
S4_VELOCITY = micropython.const(4)

## @brief State 8 - Set proportional gain
S8_Kp = micropython.const(8)

## @brief State 9 - Define set point
S9_Kd = micropython.const(9)

## @brief State 11 - For IMU calibration
S11_CAL = micropython.const(11)

## @brief State 12 - Determines whether touch calibration file loaded
S12_CALTouch = micropython.const(12)

## @brief State 13 - Checks if touch calibration file present and correct
S13_TCAL = micropython.const(13)

## @brief State 14 - Ball balancing state
S14_BallBal = micropython.const(14)

## @brief State 15 - Set integral gain
S15_I = micropython.const(15)

## @brief State 16 - Displays possible program commands
S16_HELP = micropython.const(16)

## @brief State 5 - Data Collection
#  @details State that collects 10 seconds of data and outputs it to the user
S17_DATA = micropython.const(17)



def taskUser(taskname, period, zFlag, pFlag, vFlag, position, T, Velo,mode,opmode,cFlag,calwrite,cal_byte,calread,bFlag,duty_1,duty_2,Tscreen,calFlag,readTouch,fFlag,ADCData):
    '''!@brief A generator to implement the UI task as an Finite State Machine.
        @details The task runs as a generator function and requires a task name
                 and interval to be specified. This task file receives flag
                 values altered by the user. These flags are then shared with
                 task_encoder.py where that task file can execute actions
                 defined in encoder.py. Once the desired task is completed,
                 the state is reset back to idle state (State 1).
        @param taskName The name of the task as a string.
        @param period Specifies how long the task has to perform its function
                      in microseconds. Specified as an integer.
        @param zFlag Holds True or False value to know when to change IMU mode
        @param pFlag Holds True or False value to know when position command
                     was called by the user to know the current position of the
                     encoder.
        @param vFlag Holds True or False value to know when to collect velocity
                     data
        @param position Stores current position of IMU to be read from
                        task_IMU.py
        @param T Stores time readig of task_touch.py for data collection
        @param Velo Stores angular velocity of platform
        @param mode Contains string to know whether in open or closed loop
        @param opmode Stores user selected operating mode of IMU
        @param cFlag Holds True or False to begin platform calibration
        @param calwrite Holds True or False to begin writing calibration data to
                        IMU
        @param cal_byte Stores byte data of the calibration status of IMU
        @param calread Holds True or False to begin reading IMU calibration data to
                       later write to a text file
        @param bFlag Holds True or False to begin recording platform position and
                     velocity data
        @return yield None          

    '''
    
    ## @brief State Variable
    #  @details Stores the state we are in so the program knows which code to
    #          run
    state = S0_INIT
    
    print("+-------------------------------------------------------------+")
    print("|          ME 305: Platform Controller Interface              |")
    print("+-------------------------------------------------------------+")
    print("| Use the following Commands:                                 |")
    print("| e or E | Select operating mode of IMU                       |")
    print("| a or A | Print position of platform in Euler Angles         |")
    print("| v or V | Print velocity of platform                         |")
    print("| f or F | Enable ball balance mode                           |")
    print("| o or O | Select outer loop gains to change                  |")
    print("| n or N | Select inner loop gains to change                  |")
    print("| p or p | Enter proportional control gain                    |")
    print("| d or D | Enter derivative control gain                      |")
    print("| i or I | Enter integral control gain                        |")
    print("| q or Q | Check gain values                                  |")
    print("| r or R | Run balancer mode                                  |")
    print("| g or G | Collect 10 seconds of ball balancer data           |")
    print("| s or S | Exit                                               |")
    print("| h or H | Return this help page                              |")
    print("| Ctrl+C | Terminate Program                                  |")
    print("+-------------------------------------------------------------+")
    
    # A timestamp, in microseconds, indicating when the next iteration of the
    # generator must run.
    
    ## @brief Start time
    #  @details Records the time in microseconds the program started running in 
    #          order to calculate the time passed since start
    start_time = ticks_us()
    
    ## @brief Next Cycle time
    #  @details State that returnsthe next time in the cycle
    next_time = ticks_add(start_time, period)
    
    ## @brief Initialize the serport to read data
    #  @details A (virtual) serial port object used for getting characters 
    #          cooperatively.
    serport = USB_VCP()
    
    ## @brief Python garbage collection
    #  @details To assist in freeing up memory
    gc.collect()
    
    ## @brief Empty 500 by 5 matrix of zeros
    #  @details Used in data collection of platform and ball position
    Collection = np.zeros((500,5))

     
    # The finite state machine must run indefinitely.
    while True:
        # We should only call the ticks_us() function once per iteration of the 
        # of the task to "timestamp" the task iteration.
        
        ##@brief Finds current time of timer
        current_time = ticks_us()
    
        # The finite state machine only needs to run if the interval has elapsed
        # as indicated by current_time being greater than next_time.
        if ticks_diff(current_time,next_time) >= 0:
        
            # Once the task does need to run we must prepare for the next
            # iteration by adjusting the value of next_time by adding the task
            # interval. Again we can't use normal arithmetic so the ticks_add()
            # function is used.
            next_time = ticks_add(next_time, period)
            
            # State 0 code
            if state == S0_INIT:             
                # state = S1_CMD
                filename = 'IMU_cal_coeffs.txt'
                if filename in os.listdir(): #When file exists
                    with open('IMU_cal_coeffs.txt', 'r') as f:
                          if f.readline() == '':
                              print("Calibration File Corrupted...")
                              print("To calibrate, tilt the platform to every side,")
                              print("holding for a few seconds between sides. As")
                              print("the IMU calibrates the numbers of the magnetometer,")
                              print("accelerometer, and gyroscope will increase until")
                              print("a value of 3 is reached. Once calibrated, the code")
                              print("will write a new calibration file, and run the rest")
                              print("of the code.")
                              state = S11_CAL
                              cFlag.write(True)
                              i = 0
                          else:
                              state = S1_CMD # transition to state 1
                              print('Platform orientation calibration file successfully loaded')
                              calwrite.write(True)
                              state = S13_TCAL
                              
                else:
                    print("Platform calibration required...")
                    print("To calibrate, tilt the platform to every side,")
                    print("holding for a few seconds between sides. As")
                    print("the IMU calibrates the numbers of the magnetometer,")
                    print("accelerometer, and gyroscope will increase until")
                    print("a value of 3 is reached. Once calibrated, the code")
                    print("will write a new calibration file, and run the rest")
                    print("of the code.")
                    state = S11_CAL
                    cFlag.write(True)
                    i = 0 
                
                CLi = closedloop.ClosedLoop()
                CLo = closedloop.ClosedLoop()
                
                Setting = ''
                
                # Setting gains to default value
                Kp = ''
                Kd = ''
                Ki = ''
                iKp = ''
                iKd = ''
                iKi = ''
                oKp = ''
                oKd = ''
                oKi = ''
                
                datacomplete = False
            # State 1 code
            elif state == S1_CMD:
                
                if serport.any():
                # Read one character and decode it into a string
                    ##@brief Input Character
                    # @details Reads one character and decodes it into a string
                    #          . Then it stores the input character.
                    charIn = serport.read(1).decode()
                    
                    if charIn in {'e','E'}:
                        print("i or I = IMU")
                        print("c or C = Compass")
                        print("m or M = M4G")
                        print("d or D = NDOF_FMC_OFF")
                        print("n or N = NDOF")
                        print("Select Operating Mode:")
                        state = S2_MODE
                        zFlag.write(True)
                        
                    elif charIn in {'a', 'A'}:
                        print("Platform Position [deg]:")
                        print("(X (Heading), Y (Roll), Z (Pitch))")
                        pFlag.write(True)
                        state = S3_POS
                    elif charIn in {'g', 'G'}:
                        print("You must have ball balancing mode enabled and run program to use this functionality")    
                        
                    elif charIn in {'o','O'} and mode.read() in {'BAL','BALLBAL'}:
                        print("Outer loop modification selected")
                        Setting = "Outer"
                    
                    elif charIn in {'n','N'} and mode.read() in {'BAL','BALLBAL'}:
                        print("Inner loop modification selected")
                        Setting = "Inner"
                        
                    elif charIn in {'h', 'H'}:
                            print("+-------------------------------------------------------------+")
                            print("|          ME 305: Platform Controller Interface              |")
                            print("+-------------------------------------------------------------+")
                            print("| Use the following Commands:                                 |")
                            print("| e or E | Select operating mode of IMU                       |")
                            print("| a or A | Print position of platform in Euler Angles         |")
                            print("| v or V | Print velocity of platform                         |")
                            print("| f or F | Enter ball balance mode                            |")
                            print("| o or O | Select outer loop gains to change                  |")
                            print("| n or N | Select inner loop gains to change                  |")
                            print("| p or p | Enter proportional control gain                    |")
                            print("| d or D | Enter derivative control gain                      |")
                            print("| i or I | Enter integral control gain                        |")
                            print("| q or Q | Check gain values                                  |")
                            print("| r or R | Run balancer mode                                  |")
                            print("| g or G | Collect 10 seconds of ball balancer data           |")
                            print("| s or S | Exit                                               |")
                            print("| h or H | Return this help page                              |")
                            print("| Ctrl+C | Terminate Program                                  |")
                            print("+-------------------------------------------------------------+")
                                      
                    elif charIn in {'v', 'V'}:
                        vFlag.write(True)
                        print('Angular Velocity [degrees/s]:')
                        print("(X (Heading), Y (Roll), Z (Pitch))")
                        state = S4_VELOCITY
                    
                    # elif charIn in {'b','B'}:
                    #     if mode.read() in {'BAL'}:
                    #         print('Balance mode disabled')
                    #         mode.write('NBAL')
                    #     elif mode.read() in {'','NBAL','NBALLBAL'}:
                    #         print('Balance mode enabled')
                    #         mode.write('BAL')
                    #     Setting = 0
                    
                    elif charIn in {'f', 'F'}:
                        if mode.read() in {'BALLBAL'}:
                            print('Ball balance mode disabled')
                            mode.write('NBALLBAL')
                        elif mode.read() in {'NBALLBAL'}:
                            print('Ball balance mode enabled')
                            mode.write('BALLBAL')
                    
                    elif charIn in {'p','P'} and mode.read() == 'BALLBAL' and Setting != '':
                        print('Enter proportional gain in [%s/deg]:')
                        state = S8_Kp
                        Kp = 0
                        buff = ''
                        
                    elif charIn in {'d', 'D'} and mode.read() == 'BALLBAL' and Setting != '':
                        print('Enter derivative control gain in [%/deg]')
                        state = S9_Kd
                        Kd = 0
                        buff = ''
                    
                    elif charIn in {'i', 'I'} and mode.read() == 'BALLBAL' and Setting != '':
                        print('Enter integral control gain in [%/deg]')
                        state = S15_I
                        Ki = 0
                        buff = ''
                        
                    elif charIn in {'p','P','d','D','i','I'} and Setting == '':
                        print('Please specify the type of loop before setting a gain')
                    
                    elif charIn in {'p','P','d', 'D','i', 'I','n','N','o','O'} and mode.read() in {'NBALLBAL'}:
                        print('You must have a balance mode enabled to use this functionality')
                
                    elif charIn in {'s','S'}:
                        print("Gains cleared, you are in the command window")
                        Kp = ''
                        Kd = ''
                        Ki = ''
                        iKp = ''
                        iKd = ''
                        iKi = ''
                        oKp = ''
                        oKd = ''
                        oKi = ''
                    elif charIn in {'q','Q'}:
                        print("Gain value assignments:")
                        print("Inner:")
                        print("Kp|",iKp)
                        print("Kd|",iKd)
                        print("Outer:")
                        print("Kp|",oKp)
                        print("Kd|",oKd)
                        print("Ki|",oKi)
                    
                    elif charIn in {'r','R'}:
                        
                        if '' not in [iKp,iKd,oKp,oKd,oKi] and mode.read() == 'BALLBAL':
                            print('Ball balancing operational')
                            bFlag.write(True)
                            fFlag.write(True)
                            
                            CLi.setgain(iKp,iKd,0)
                            CLo.setgain(oKp,oKd,oKi)
                            state = S14_BallBal
                        
                        elif '' in [iKp,iKd,oKp,oKd,oKi]:
                            print("You have not specified all of the required gains")
                            print("Press q or Q to show gains currently set")
                            
                        
                    else:
                        print(f"You typed {charIn} from state 1 at t={ticks_diff(current_time,start_time)/1e6}[s].")

                        # Platform 4 Settings
                        # Angle offset: y: +1, z: +1
                        # iKp = 5
                        # iKd = .3
                        # oKp = 0.25
                        # oKd = 0.03
                        # oKi = 0.0                        
                              
                        # Platform 5 Settings
                        # iKp = 5.3
                        # iKd = .3
                        # oKp = 0.25
                        # oKd = 0.035
                        # oKi = 0.0                        
                    
            elif state == S2_MODE:
                if serport.any():
                    charIn = serport.read(1).decode()
                    
                    # contains different IMU operating modes contained in bit
                    # values
                    if charIn in {'i', 'I'}:
                        opmode.write(0b1000)
                        mo = 'IMU' 
                    elif charIn in {'c', 'C'}:
                        opmode.write(0b1001)
                        mo = 'COMPASS'
                    elif charIn in {'m', 'M'}:
                        opmode.write(0b1010)
                        mo = 'M4G'
                    elif charIn in {'d', 'D'}:
                        opmode.write(0b1011)
                        mo = 'NDOF_FMC_OFF'
                    elif charIn in {'n', 'N'}:
                        opmode.write(0b1100)
                        mo = 'NDOF'
                    else:
                        print("Not an available mode")
                
                elif not zFlag.read():
                    
                    print(f"{mo} mode selected")
                    state = S1_CMD
                    
            elif state == S3_POS:
                if not pFlag.read():
                    print(f"{(position.read())}")
                    state = S1_CMD 
                      
            
            elif state == S4_VELOCITY:
                
                if not vFlag.read():
                    print(Velo.read())
                    state = S1_CMD
            
                                                
            elif state == S8_Kp:
                
                if serport.any():
        
                    charIn = serport.read(1).decode()
                    
                    if charIn.isdigit():
                        buff += charIn
                    elif charIn == '-':
                        if len(buff) == 0:
                            buff = charIn
                        else:
                            print('Incorrect Syntax')
                    elif charIn == '.':
                        if buff.count('.') == 0:
                            buff += charIn
                        else:
                            print('You can only put one decimal for proportional gain')
                    elif charIn in {'\x7f', '\b', '\x08'}:
                        if len(buff) > 0:
                            buff = buff[0:-1]
                                             
                    elif charIn in {'\r' , '\n'}:
                        
                        serport.write('\n')
                        
                        if len(buff) == 0:
                            Setting = ''
                            print("Gain remains unchnanged")
                        else:
                            Kp = float(buff)
                            if Kp > 10:
                                Kp = 10 
                            elif Kp < 0:
                                Kp = 0
                            else:
                                if Setting == 'Inner': 
                                    iKp = Kp
                                    Setting = ''
                                    print(Setting,"Kp =",Kp,"[%s/rad]")
                                    
                                elif Setting == 'Outer':
                                    oKp = Kp
                                    Setting = ''
                                    print(Setting,"Kp =",Kp,"[%s/rad]")
                            
                        buff = ''
                        state = S1_CMD
                        
                    serport.write(charIn)
            
            
            elif state == S9_Kd:
                if serport.any():
        
                    charIn = serport.read(1).decode()
                    
                    if charIn.isdigit():
                        buff += charIn
                    elif charIn == '-':
                        if len(buff) == 0:
                            buff = charIn
                        else:
                            print('Incorrect Syntax')
                    elif charIn == '.':
                        if buff.count('.') == 0:
                            buff += charIn
                        else:
                            print('You can only put one decimal for derivative gain')
                    elif charIn in {'\x7f', '\b', '\x08'}:
                        if len(buff) > 0:
                            buff = buff[0:-1]
                                             
                    elif charIn in {'\r' , '\n'}:
                        
                        serport.write('\n')
                        if len(buff) == 0:
                            Setting = ''
                            print("Gain remains unchnanged")
                            
                        else:
                            Kd = float(buff)
                            if Kd > 2:
                                Kd = 2 
                            elif Kd < 0:
                                Kd = 0
                            else:
                                if Setting == 'Inner': 
                                    iKd = Kd
                                    Setting = ''
                                    print(Setting,"Kd =",Kd,"[%s/deg]")
                                    
                                elif Setting == 'Outer':
                                    oKd = Kd
                                    Setting = ''
                                    print(Setting,"Kd =",Kd,"[%s/deg]")
                        buff = ''
                        state = S1_CMD
                        
                    serport.write(charIn)
            
            elif state == S15_I:
                if serport.any():
        
                    charIn = serport.read(1).decode()
                    
                    if charIn.isdigit():
                        buff += charIn
                    elif charIn == '-':
                        if len(buff) == 0:
                            buff = charIn
                        else:
                            print('Incorrect Syntax')
                    elif charIn == '.':
                        if buff.count('.') == 0:
                            buff += charIn
                        else:
                            print('You can only put one decimal for integral gain')
                    elif charIn in {'\x7f', '\b', '\x08'}:
                        if len(buff) > 0:
                            buff = buff[0:-1]
                                             
                    elif charIn in {'\r' , '\n'}:
                        
                        serport.write('\n')
                        if len(buff) == 0:
                            # print("Integral control gain set to zero")
                            Setting = ''
                            print("Gain remains unchnanged")
                            # print("Kd = ",Kd.read(),"[%s/deg]")
                            
                        else:
                            Ki = float(buff)
                            if Ki < 0:
                                Ki = 0
                            elif Ki > 2:
                                Ki = 2
                            else:
                                if Setting == 'Inner': 
                                    iKi = Ki
                                    Setting = ''
                                    print(Setting,"Ki =",Ki,"[%/deg]")
                                    
                                elif Setting == 'Outer':
                                    oKi = Ki
                                    Setting = ''
                                    print(Setting,"Ki =",Ki,"[%/deg]")
                        buff = ''
                        state = S1_CMD
                        
                    serport.write(charIn)        
            
                    
            
            elif state == S11_CAL and cal_byte.read() != '':
                mag_stat = cal_byte.read()  & 0b00000011
                acc_stat = (cal_byte.read() & 0b00001100)>>2
                gyr_stat = (cal_byte.read() & 0b00110000)>>4
                sys_stat = (cal_byte.read() & 0b11000000)>>6 
                
                mag_stat_s = "Mag:" + str(mag_stat) + ",  "
                acc_stat_s = "Acc:" + str(acc_stat) + ",  "
                gyr_stat_s = "Gyr:" + str(gyr_stat) + ",  "
                sys_stat_s = "Sys:" + str(sys_stat)
                output = (mag_stat_s, acc_stat_s, gyr_stat_s, sys_stat_s)
                
                serport.write("\033[2K")
                serport.write("\x0D")
                string = ''.join(output)
                serport.write(string)
                
                # After 100 iterations, the system will be calibrated
                if i >= 100 and mag_stat == 3 and acc_stat == 3 and gyr_stat == 3:
                    serport.write("\n")
                    print("System Calibrated")
                    calread.write(True)
                    cFlag.write(False)
                    state = S0_INIT
                i += 1    
                    
            elif state == S12_CALTouch:
                if not calFlag.read():
                    print("Touch panel calibration file loaded")
                    state = S1_CMD
                
            elif state == S13_TCAL:
                filename = 'Touch_Calibration.txt'
                if filename in os.listdir(): #When file exists
                    with open('Touch_Calibration.txt', 'r') as f:
                          if f.readline() == '':
                              print("Touch Panel Calibration File Corrupted...")
                              print("To calibrate, follow the instructions provided.")
                              print("First, touch the center of the panel.")
                              state = S12_CALTouch
                              calFlag.write(True)
                              
                          else:
                              state = S1_CMD # transition to state 1
                              print('Touch panel calibration file successfully loaded')
                              readTouch.write(True)
      
                else:
                    print("Touch panel calibration required...")
                    print("To calibrate, follow the instructions provided.")
                    print("First, touch the center of the panel.")
                    state = S12_CALTouch
                    calFlag.write(True)
                    
            elif state == S14_BallBal:
                
                #x - direction
                ThetaRef = CLo.run(0,-ADCData.read()[0],-ADCData.read()[2],0) # Outer Loop
                duty_1.write(CLi.run(ThetaRef, float(position.read()[1]), -float(Velo.read()[1]), 1)) # Inner Loop
                
                # y - direction
                ThetaRef = CLo.run(0,ADCData.read()[1],ADCData.read()[3],0) # Outer Loop
                duty_2.write(CLi.run(ThetaRef, -float(position.read()[2]), -float(Velo.read()[0]), 1)) # Inner Loop
                
                if serport.any():
        
                    charIn = serport.read(1).decode()
                    
                    if charIn in {'s','S'}:
                        fFlag.write(False)
                        bFlag.write(False)
                        mode.write('NBALLBAL')
                        
                        duty_1.write(0)
                        duty_2.write(0)
                        state = S1_CMD
                        print('You have returned to the command window, balance mode has been disabled')  
                    
                    elif charIn in {'g','G'}:
                        print("Data Collection Initialized")
                        gstart_time = ticks_ms()
                        state = S17_DATA
                        i = 0
                        k = 0
        
                elif datacomplete == True and i < len(Collection):
                    print(f"{(Collection[i,0]):.3f}, {Collection[i,1]:.2f}, {Collection[i,2]:.2f}, {Collection[i,3]:.2f}, {Collection[i,4]:.2f}")
                    i += 1
                    
            elif state == S17_DATA:
                ThetaRef = CLo.run(0,-ADCData.read()[0],-ADCData.read()[2],0) # Outer Loop - x
                duty_1.write(CLi.run(ThetaRef, float(position.read()[1]), -float(Velo.read()[1]), 1)) # Inner Loop - x
                
                # y - direction
                ThetaRef = CLo.run(0,ADCData.read()[1],ADCData.read()[3],0) # Outer Loop - y
                duty_2.write(CLi.run(ThetaRef, -float(position.read()[2]), -float(Velo.read()[0]), 1)) # Inner Loop - y
                
                # This only writes data every other run
                if k%2 == 0:
                    Collection[i,0] = float(ticks_diff(T.read(),gstart_time))/1000
                    Collection[i,1] = ADCData.read()[0] # x - postion
                    Collection[i,2] = ADCData.read()[2] # x_dot - velocity
                    Collection[i,3] = float(position.read()[1])
                    Collection[i,4] = float(Velo.read()[1])
                    i += 1
                    
                    if i == len(Collection):
                        datacomplete = True
                        i = 0
                        k = 0
                        state = S14_BallBal
                k += 1
                
                    
            else:
                raise ValueError(f"Invalid state value in {taskName}: State {state} does not exist")
        
            # After a valid run of the state machine we can yield the state.
            # This yielded value could be stored in the main loop below to trace
            # the state transitions.
            yield None
        
        # If the time has not come yet to run the task we can just exit early by
        # yielding nothing.
        else:
            yield None


