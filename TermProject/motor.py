'''!
@file motor.py
@brief Creates Motor Class
@details Motor Class sets the appropriate channels and pulse width modulation.
               Receives duty cycle to set the speed of the respective motor
               based on the set channels. Handles both positive and negative
               duty cycles to change the direction of rotation of the motor
               shaft

@date (2/17/2022)
@author Ryan Dean
@author Zachary Hendrix

'''



import pyb
from pyb import Pin, Timer

class Motor:
    '''!
    @brief Motor Class
    @details Consists of methods and attributes to set the speed of a motor.
    '''
    def __init__ (self, channel, PWM_tim, IN1_pin, IN2_pin):
        '''!
        @brief Initializes a motor object
        @details Initializes a motor object of the driver class. This uses the 
                 channels built-in to the motor and sets the proper parameters
                 to cause the motor to spin.
        
        @param channel Contains channel number for specific motor used
        @param PWM_tim Contains the the timer value for proper PWM
        @param IN1_pin Object to interface with pin that determines rotation
               direction
        @param IN2_pin Object to interface with pin that determines rotation
               direction
         
        @return None          
        '''
        
        ## @brief Object to contain pin tied to motor for PWM control
        self.in1 = PWM_tim.channel(channel[0], pyb.Timer.PWM_INVERTED, pin=IN1_pin)
        
        ## @brief Object to contain pin tied to motor for PWM control
        self.in2 = PWM_tim.channel(channel[1], pyb.Timer.PWM_INVERTED, pin=IN2_pin)
        
    
    def set_duty(self, duty):
        '''!
        @brief Sets duty cycle of motors
           
        @param duty Contains float or integer value for duty cycle of motor.
        
        @return None
        '''
        if duty <= 0:
            
            self.in1.pulse_width_percent(0)
            self.in2.pulse_width_percent(abs(duty))
                      
        elif duty > 0:   
            
            self.in1.pulse_width_percent(duty)
            self.in2.pulse_width_percent(0)
        
