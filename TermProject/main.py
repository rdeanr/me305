'''!
@file main.py
@brief Initializes values and manages tasks
@details This is the main file to run the tasks required to interact with the 
         encoder. It will continually call the specified task until the user
         interrupts the program by ending it with Keyboard Interrupt (Ctrl-C).
         Variables shared between tasks are initialized here.
         
         Code repository for this lab:
         https://bitbucket.org/rdeanr/me305/src/master/Term_Project/
         
         See
@ref     TP 
         for state transition diagrams, task diagrams, plots and video
         demonstrating the balancing of the ball on the platform along with how
         the UI was implemented.         

@author Ryan Dean
@author Zachary Hendrix
@date 3/17/2022
'''

import shares
from time import ticks_diff, ticks_add, ticks_us
from pyb import USB_VCP
from pyb import Pin
import micropython
import pyb
import BNO_driver
from task_IMU import taskIMU
import motor
from task_motor import taskMotor
from task_user import taskUser
import array as arr
from ADC_driver import ADC_Driver
from task_touch import taskTouch

## @brief Signal that the button z has been pressed
#  @details Signals that the user wants the IMU mode to change
zFlag = shares.Share(False)

## @brief Signal that the button p has been pressed
#  @details Shared object signaling that the user wants to collect platform 
#           position data
pFlag = shares.Share(False)

## @brief Signal that the button v has been pressed
#  @details Shared object signaling that the user wants to collect platform 
#           velocity data
vFlag = shares.Share(False)


## @brief Stores angular position of platform for transfer between files.
#  @details Used to communicate between task_IMU.py and task_user.py in order 
#           to pass the position across those files.
position = shares.Share(0)


## @brief Stores a single data point reading of touch panel time
T = shares.Share(0)

## @brief Shared variable duty cycle for motor 1
duty_1 = shares.Share(0)

## @brief Shared variable duty cycle for motor 2
duty_2 = shares.Share(0)

##@brief Shared variable angular velocity of platform
Velo = shares.Share(0)


## @brief Shared variable that holds the current mode. 
mode = shares.Share('NBALLBAL')


## @brief Shared variable that holds the IMU operating mode
opmode = shares.Share('')

## @brief Shared variable that holds the IMU operating mode
cFlag = shares.Share(False)

## @brief Shared variable that holds the IMU operating mode
calwrite = shares.Share(False)

## @brief Shared variable that holds the IMU operating mode
cal_byte = shares.Share('')

## @brief Shared variable to begin reading IMU calibration data
calread = shares.Share(False)

## @brief Shared variable to begin platofrm position and velocity
#         data collection
bFlag = shares.Share(False)

## @brief Shared variable to begin touch panel calibration 
calFlag = shares.Share(False)

## @brief Shared variable to begin reading touch panel calibration coefficient 
#         data
readTouch = shares.Share(False)

## @brief Shared variable to begin collection of touch panel position data
fFlag = shares.Share(False)

## @brief Share variable to store touch panel data
ADCData = shares.Share([0,0,0,0])

## @brief Pin object
pinxm = Pin.cpu.A1

## @brief Pin object
pinxp = Pin.cpu.A7

## @brief Pin object
pinym = Pin.cpu.A0

## @brief Pin object
pinyp = Pin.cpu.A6

## @brief Class object to run ADC_Driver.py file
Tscreen = ADC_Driver(pinxm,pinxp,pinym,pinyp)

## @brief Holds task information and settings to pass into task_user.py
task1 = taskUser('taskUser', 10_000, zFlag, pFlag, vFlag, position,T, Velo, mode,opmode,cFlag,calwrite,cal_byte,calread,bFlag,duty_1,duty_2,Tscreen,calFlag,readTouch,fFlag,ADCData)

## @brief Holds task information and settings to pass into task_IMU.py
task2 = taskIMU('taskIMU',5_000, zFlag, pFlag, vFlag, position, Velo,opmode,cFlag,calwrite,cal_byte,calread,bFlag)

## @brief Objec to contain and set Nucleo timing settings  
PWM_tim = pyb.Timer(3, freq = 20000)

## @brief Obect to contain proper settings to run motor 1
motor_1 = motor.Motor([1, 2], PWM_tim, pyb.Pin.cpu.B4, pyb.Pin.cpu.B5)

## @brief Obect to contain proper settings to run motor 2
motor_2 = motor.Motor([3, 4], PWM_tim, pyb.Pin.cpu.B0, pyb.Pin.cpu.B1)



## @brief Holds task information and settings to pass into task_motor.py
task3 = taskMotor('taskMotor',3_000, motor_1, motor_2, duty_1, duty_2, mode)

## @brief Holds task information and settings to pass into task_touch.py
task4 = taskTouch('taskTouch',2_000,Tscreen,calFlag,readTouch,fFlag,ADCData,T)

## @brief List of all tasks in program
#  @details This list will be used to cycle through every task in the list.
taskList = [task1, task2, task3, task4]


# The system should run indefinitely until the user interrupts program flow
# by using a Ctrl-C to trigger a keyboard interrupt. The tasks are run by
# using the next() function on each item in the task list.
while True:
    try:
        for task in taskList:
            next(task)
    
    except KeyboardInterrupt:
        break
print('Program Terminating')

