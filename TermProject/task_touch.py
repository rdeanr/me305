'''!
@file task_touch.py
@brief Directly interfaces with touch panel
@details Task file to used to store calibartion data of touch panel, that is
         stored in a list, pushed through ADC_driver.py, and storing the
         calibration coefficient data to be interpretted by task_user.py and
         written to a text file.
         
         See
@ref     TP 
         for state transition diagrams, task diagrams, plots and video
         demonstrating the balancing of the ball on the platform along with how
         the UI was implemented.  

@author Ryan Dean
@author Zachary Hendrix
@date 3/17/2022
'''


import pyb
from time import ticks_diff, ticks_add, ticks_us, ticks_ms
from pyb import USB_VCP
from shares import Share
import array as arr
import ADC_driver



def taskTouch(taskname, period,Tscreen,calFlag,readTouch,fFlag,ADCData,T):
    '''!
    @brief Generator function to inteface with IMU
    @details Generator function receives values of certain flags that
             correspond to a desired action from the user. This then triggers
             the function to interact with the encoder.py drive file, 
             performing the necessary action or storing the value for the user
             to then see with their interface. 
    @param taskname Name of the task being performed.
    @param period Specifies how long the task has to perform its function.
                  received as microseconds. Specified as an integer.
    @param Tscreen Object to run ADC_Driver class
    @param calFlag Holds True or False to begin calibration procedures
    @param readTouch Holds True or False to read touch panel calibration
                     coefficient data 
    @param fFlag Holds True or False to begin collection of touch panel
                 position data
    @param ADCData Shared variable storing touch panel position data
    @param T Shared variable holding timing data for data collection in
             task_user.py

    @return yields None
    '''
    
    
    ## @brief Start time
    #  @details Assigns a variable with the time that the taskTouch
    #           function is called in microseconds.
    start_time = ticks_us()
    
    ## @brief Future moment in time
    #  @details Assigns a variable with the time that the taskTouch
    #           function is called plus the specified period the task is
    #           allowed to run.
    next_Time = ticks_add(start_time, period)
    
    ## @brief Empty array of zeros
    #  @details Iterated though to store calibration data
    CalList = arr.array('f',15*[0])
    p = 0
    
    while True:
        # brief Current time of Nucleo
        current_Time = ticks_us()
               
        if ticks_diff(current_Time,next_Time) >= 0:
            next_Time = ticks_add(next_Time,period)
            
            if calFlag.read():
                if p == 0 and Tscreen.scanZ() == True:
                    CalList[0] = Tscreen.scanX()
                    CalList[1] = Tscreen.scanY()
                    CalList[2] = 1
                    p = 1
                    start = ticks_us()
                    pause = 1_000_000
                    new = ticks_add(start,pause)
                    
                    print("Touch the top left corner of the grid")
                elif p == 1 and Tscreen.scanZ() == True and ticks_diff(current_Time,new) >= 0:
                    CalList[3] = Tscreen.scanX()
                    CalList[4] = Tscreen.scanY()
                    CalList[5] = 1
                    p = 2
                    start = ticks_us()
                    new = ticks_add(start,pause)
                    print("Touch the top right corner of the grid")
                elif p == 2 and Tscreen.scanZ() == True and ticks_diff(current_Time,new) >= 0:
                    CalList[6] = Tscreen.scanX()
                    CalList[7] = Tscreen.scanY()
                    CalList[8] = 1
                    p = 3
                    start = ticks_us()
                    new = ticks_add(start,pause)
                    print("Touch the bottom right corner of the grid")
                    # calFlag.write(False)
                elif p == 3 and Tscreen.scanZ() == True and ticks_diff(current_Time,new) >= 0:
                    CalList[9] = Tscreen.scanX()
                    CalList[10] = Tscreen.scanY()
                    CalList[11] = 1
                    p = 4
                    start = ticks_us()
                    new = ticks_add(start,pause)
                    print("Touch the bottom left corner of the grid")
                elif p == 4 and Tscreen.scanZ() == True and ticks_diff(current_Time,new) >= 0:
                    CalList[12] = Tscreen.scanX()
                    CalList[13] = Tscreen.scanY()
                    CalList[14] = 1
                    p = 5
                    start = ticks_us()
                    new = ticks_add(start,pause)
                    print("Touch Sensor Calibrated")
                elif p == 5:
                    # Tscreen.calibrate(CalList)
                    B = Tscreen.calibrate(CalList)
                    Tscreen.writeBeta()
                    calFlag.write(False)
                    # add flag to read and write beta data to a text file
            elif readTouch.read():
                Tscreen.readBeta()
                readTouch.write(False)
                
            elif fFlag.read():
                ADCData.write(Tscreen.scanXYZ())
                T.write(ticks_ms())
                
                
                
            
            
                
                
            
            
            
            yield None
        
        else:
            yield None

            
            
            
            
            
            