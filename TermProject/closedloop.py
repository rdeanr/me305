'''!
@file closedloop.py
@brief Creates ClosedLoop Class
@details Closed Loop Class is used to store relevent methods for the cascaded
         controller used to balance the ball on the platform. An inner and
         outer loop instance are created with the output of the outer loop
         feeding into the inner loop. The final result is an output duty cycle.
         
         
@date (1/23/2022)
@author Ryan Dean
@author Zachary Hendrix
'''

import pyb
from time import ticks_us, ticks_diff

class ClosedLoop:
    '''! 
    @brief ClosedLoop Class
    @details Consists of methods and variables to calculate the duty cycle to
             automatically balance a ball. The task_user.py runs the instances
             of this class for cascaded control
             
    '''
    def __init__ (self):
        '''!
        @brief Initializes ClosedLoop class
        @return None
        '''
        ## @brief Proportional gain
        #  @details Initializes to be used in various other methods. Initially 
        #           0.
        self.Kp = 0
        
        ## @brief Derivative gain
        #  @details Initializes to be used in various other methods. Initially 
        #           0.
        self.Kd = 0
        
        ## @brief Integral gain
        #  @details Initializes to be used in various other methods. Initially 
        #           0.
        self.Ki = 0
        
        ## @brief Start time of when class instance first initialized
        self.start = ticks_us()
        
        ## @brief Contains accumulation of angular position error
        self.accum = 0
    
    def setgain(self,Kp,Kd,Ki):
        '''!
        @brief Sets gains to user input from task_user.py
        @param Kp Proportional gain 
        @param Kd Derivative gain
        @param Ki Integral gain
        @return None
        '''
        self.Kp = Kp
        self.Kd = Kd
        self.Ki = Ki
        
    def run(self,Pref,Pmeasured,Vmeasured,Kisign):
        '''!
        @brief Calculates output of each closed loop
        @details The output of the outer closed loop will provide the reference
                 position to be used in the inner loop. The final output is the
                 duty cycle.
        @param Pref Reference position
        @param Pmeasured Measured position of ball or platform
        @param Vmeasured Measured velocity of ball or platform
        @param Kisign Controls positive or negative magnitude of integral gain
        @return Reference position of platform for first loop, then duty cycle
                for the second loop
        '''
        # end = ticks_us()
        # self.start = end
        
        self.accum += Pmeasured
        
        ## @brief Final value of closed loop after application of various gains
        self.CLvalue = self.Kp*(Pref + Pmeasured) + self.Kd*Vmeasured + Kisign*self.Ki*self.accum*(1/10)
        
        return self.CLvalue
        
    
    # def set_Kp(self, Kp_input):
    #     '''!
    #     @brief Changes the Gain Value
    #     @details Changes the Gain  value for the closed loop duty cycle 
    #             calculation
    #     @param Kp_input Input gain from user for integral control
    #     @return None
    #     '''
    #     self.Kp = Kp_input
        
    # def set_Kd(self, Kd_input):
    #     self.Kd = Kd_input
        
        
        