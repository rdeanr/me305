'''!
@file task_IMU.py
@brief Directly interfaces with IMU
@details Called at a frequency of 200Hz, this task file uses the class 
         BNO_driver within the BNO_driver.py drive file to interface with the 
         IMU. Continually updating the position of the IMU, this task 
         file uses flag values altered by the user in task_user.py to execute 
         actions defined in BNO_dirver.py. Once the action is completed, these 
         flags are reset.

         See
@ref     TP 
         for state transition diagrams, task diagrams, plots and video
         demonstrating the balancing of the ball on the platform along with how
         the UI was implemented. 
 
@author Ryan Dean
@author Zachary Hendrix
@date 3/17/2022
'''

import BNO_driver
import pyb
from time import ticks_diff, ticks_add, ticks_us, ticks_ms
from pyb import USB_VCP
from shares import Share
import array as arr



def taskIMU(taskname, period, zFlag, pFlag, vFlag, position,Velo,opmode,cFlag,calwrite,cal_byte,calread,bFlag):
    '''!
    @brief Generator function to inteface with IMU
    @details Generator function receives values of certain flags that
             correspond to a desired action from the user. This then triggers
             the function to interact with the encoder.py drive file, 
             performing the necessary action or storing the value for the user
             to then see with their interface. 
    @param taskname Name of the task being performed.
    @param period Specifies how long the task has to perform its function.
                  received as microseconds. Specified as an integer.
    @param zFlag Holds True or False value to know when to change IMU mode
    @param pFlag Holds True or False value to know when position command was
           called by the user to know the angular position of platform in
           Euler angles.
    @param vFlag Holds True or False value to know when to collect velocity
                 data
    @param position Stores current angular position of platformto be 
                    transferred to task_user.py
    @param Velo Stores angular velocity of platform
    @param opmode Stores user selected operating mode of IMU
    @param cFlag Holds True or False to begin platform calibration
    @param calwrite Holds True or False to begin writing calibration data to
                    IMU
    @param cal_byte Stores byte data of the calibration status of IMU
    @param calread Holds True or False to begin reading IMU calibration data to
                   later write to a text file
    @param bFlag Holds True or False to begin recording platform position and
                 velocity data
    @return yields None
    '''
    
    
    ## @brief Start time of IMU task
    #  @details Assigns a variable with the time that the taskEncoder
    #           function is called in microseconds.
    start_time = ticks_us()
    
    ## @brief Future moment in time
    #  @details Assigns a variable with the time that the taskEncoder
    #           function is called plus the specified period the task is
    #           allowed to run.
    next_Time = ticks_add(start_time, period)
    
    ## @brief Creates an object using the Encoder class of encoder.py
    #  @details Creates a object using the Encoder class of encoder.py and
    #           specifies the appropriate pins to receive data and the 
    #           appropriate Timer number to receive time data.
    IMU = BNO_driver.BNO055()  
    
    
    
    
    while True:
        # Current time of Nucleo
        current_Time = ticks_us()
        
               
        if ticks_diff(current_Time,next_Time) >= 0:
            next_Time = ticks_add(next_Time,period)
                       
            if zFlag.read():
                if opmode.read() != '':
                    IMU.op_mode(opmode.read())
                    opmode.write('')
                    zFlag.write(False)
            
            elif pFlag.read():
                position.write(IMU.read_Euler())
                pFlag.write(False)
                pass
                
            elif vFlag.read():
                Velo.write(IMU.read_angvelo())
                vFlag.write(False)
                
            
            elif bFlag.read():
                position.write(IMU.read_Euler())
                Velo.write(IMU.read_angvelo())              
            
            elif cFlag.read():
                cal_byte.write(IMU.calib_status())
                cFlag.write(False)
            
            elif calwrite.read():
                IMU.calib_coeff_write()
                calwrite.write(False)
            
            elif calread.read():
                IMU.calib_coeff_read()
                calread.write(False)
                
            yield None
            
        else:
            yield None
    

