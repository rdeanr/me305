'''!
@file ADC_driver.py
@brief Creates ADC_Driver class
@details This class directly interfaces with the touch panel and read the x and
         y position and velocity of the ball. Alpha-beta filtering is then 
         implemented to help process the signals and reduce noise. It can also 
         calibrate the platform and write the calibration coefficients to a 
         text file. It can also read said text file.

@author Ryan Dean
@author Zachary Hendrix
@date 3/17/2022
'''

import pyb
from pyb import Pin
from pyb import ADC
import time
from ulab import numpy
from time import ticks_us, ticks_diff
from pyb import USB_VCP

# xm = PA1
# xp = PA7
# ym = PA0
# yp = PA6


class ADC_Driver:
    '''! 
    @brief ADC_Driver Class
    @details Class to directly interface with touch panel. See file details for
             more details. Pin configurations change depending on coordinate
             reading.
             
    '''
    def __init__ (self,xm_pin,xp_pin,ym_pin,yp_pin):
        '''!
        @brief Initializes ADC_Driver class
        @details Initializes pin objects, calibration coefficients, and
                 estimated position/velocities
        @param xm_pin Contains pin object to set voltage to read position.
                      Connected to ground or floated.
        @param xp_pin Contains pin object to set voltage to read position.
                      Connected to 3.3V or configured for ADC
        @param ym_pin Contains pin object to set voltage to read position.
                      Connected to ground or floated.
        @param yp_pin Contains pin object to set voltage to read position.
                      Connected to 3.3V or configured for ADC
        @return None
        '''
        
        ## @brief Pin object
        self.xm = xm_pin
        
        ## @brief Pin object
        self.xp = xp_pin
        
        ## @brief Pin object
        self.ym = ym_pin
        
        ## @brief Pin object
        self.yp = yp_pin
        
        ## @brief Calibration coefficient matrix
        self.Beta = numpy.array([1,0,0,0,1,0]).reshape((2,3))
        
        ## @brief Measured time in microseconds when class first initialized
        self.oldTime = ticks_us()
        
        ## @brief Measured time in microseconds when scanXYZ() method ran
        self.newTime = 0
        
        ## @brief Difference in time between each class call
        self.deltatime = ticks_diff(self.newTime,self.oldTime)
        
        ## @brief Estimated x-position of ball
        self.xest = 0
        
        ## @brief Estimated x-direction velocity of ball
        self.vxest = 0
        
        ## @brief Estimated y-position of ball
        self.yest = 0
        
        ## @brief Estimated y-direction velocity of ball
        self.vyest = 0
        
        ## @brief Alpha value for alpha-beta filter
        self.A = .85
        
        ## @brief Beta value for alpha-beta filter
        self.B = 0.35
        
        ## @brief x-direction velocity of ball
        self.vx = 0
        
        ## @brief y-direction velocity of ball
        self.vy = 0
        
        ## @brief Predicted x-position of ball
        self.xnext = 0
        
        ## @brief Predicted x-direction velocity of ball
        self.vxnext = 0
        
        ## @brief Predicted y-position of ball
        self.ynext = 0
        
        ## @brief Predicted y-direction velocity of ball
        self.vynext = 0
        
        ## @brief Index for iteration
        #  @details Controls when loss of ball contact, causes platform to self
        #           balance
        self.ii = 0
        
    def scanX (self):
        '''!
        @brief Configures pins read x-position of ball
        @return X-position of ball
        '''
        Pin(self.xm, Pin.OUT_PP).low()
        Pin(self.xp, Pin.OUT_PP).high()
        Pin(self.ym, Pin.IN)
        posX = (ADC(self.yp).read()/4096)*176 - 88 # units of mm
        return posX
    
    def scanY(self):
        '''!
        @brief Configures pins to read y-position of ball
        @return Y-position of ball
        '''
        Pin(self.xm, Pin.IN)
        Pin(self.ym, Pin.OUT_PP).low()
        Pin(self.yp, Pin.OUT_PP).high()
        posY = (ADC(self.xp).read()/4096)*100 - 50 # units of mm
        return posY
        
    def scanZ(self):
        '''!
        @brief Determines whether ball is in contact with touch sensor
        @return True(contact) or False(no contact)
        '''
        Pin(self.xm, Pin.OUT_PP).low()
        Pin(self.yp, Pin.OUT_PP).high()
        
        # Checking if xp is 0V
        Pin(self.ym, Pin.IN)
        posXp = ADC(self.xp).read()
        
        # Checking if ym is 0V
        Pin(self.xp, Pin.IN)
        posYm = ADC(self.ym).read()
        
        # This values need to change when we implement the filter?
        if posXp < 50 and posYm > 4000:
            return False
        
        else:
            return True
        
    def scanXYZ(self):
        '''!
        @brief Finds x,y, and contact measurements of ball
        @details Calculates position and velocity of ball using alpha-beta
                 filtering.
        @return x and y position and velocities of ball
        '''
        
        #Copying the previous scan functions saves time by ~70us
        ##############
        Pin(self.xm, Pin.OUT_PP).low()
        Pin(self.xp, Pin.OUT_PP).high()
        Pin(self.ym, Pin.IN)
        
        ## @brief X-measurement
        ADCx = (ADC(self.yp).read()/4096)*176 - 88 # units of mm   
        
        #################################################################
        Pin(self.xm, Pin.IN)
        Pin(self.ym, Pin.OUT_PP).low()
        Pin(self.yp, Pin.OUT_PP).high()
        
        ## @brief Y-measurement
        ADCy = (ADC(self.xp).read()/4096)*100 - 50 # units of mm
        
        #################################################################
        Pin(self.xm, Pin.OUT_PP).low()
        Pin(self.yp, Pin.OUT_PP).high()
        
        # Checking if xp is 0V
        Pin(self.ym, Pin.IN)
        
        ## @brief selts xp Pin to ADC configuration 
        posXp = ADC(self.xp).read()
        
        # Checking if ym is 0V
        Pin(self.xp, Pin.IN)
        
        ## @brief selts ym Pin to ADC configuration 
        posYm = ADC(self.ym).read()
        
        if posXp < 50 and posYm > 4000:
            z = False
        
        else:
            z = True
        # z = self.scanZ()
        ##############
        
        # Run System Through Calibration
        
        ## @brief 3 by 1 matrix of ball position measurements
        data = numpy.array([ADCx,ADCy,1]).reshape((3,1))
        
        ## @brief Matrix multiplication of measurements with calibration
        #         matrix
        XY = numpy.dot(self.Beta,data).transpose()
        
        # Finds Change in Time
        self.newTime = ticks_us()
        self.deltatime = ticks_diff(self.newTime,self.oldTime)/1000000
        self.oldTime = self.newTime
        
        if z:
            self.ii = 0
            #alpha-beta filter
                #x
            self.vx = self.vxest
            dx = XY[0,0] - self.xest
            self.xnext = self.xest + self.A * (dx) + self.deltatime * self.vxest
            self.vxnext = (self.B / self.deltatime) * (dx) + self.vxest
            self.xest = self.xnext
            self.vxest = self.vxnext
            
                #y
            self.vy = self.vyest
            dy = XY[0,1] - self.yest
            self.ynext = self.yest + self.A * (dy) + self.deltatime * self.vyest
            self.vynext = (self.B / self.deltatime) * (dy) + self.vyest
            self.yest = self.ynext
            self.vyest = self.vynext
            
            return [XY[0,0],XY[0,1],self.vxnext,self.vynext]
        else:
            self.ii += 1
            if self.ii < 50:   
                return [self.xnext,self.ynext,self.vxnext,self.vynext]
            else:
                return [0,0,0,0]
    
    def calibrate(self, x):
        '''!
        @brief Calibrates touch panel
        @details Receives list of calibration measurements to calculate
                 calibration coefficient matrix
        @param x List of calibration measurements
        @return None
        '''
        
        ## @brief Calibration position measurements
        X = numpy.array(x).reshape((5,3))
        
        ## @brief Expected position measurents
        Y = numpy.array([0,0,-80,40,80,40,80,-40,-80,-40]).reshape((5,2))
        
        XTX = numpy.array(numpy.dot(X.transpose(),X))
        
        ## @brief Calibration coefficient matrix
        beta = numpy.dot(numpy.dot(numpy.linalg.inv(XTX),X.transpose()),Y)
        self.Beta = beta.transpose()
        
        
    
    def writeBeta(self):
        '''!
        @brief Writes calibration coefficient matrix to text file
        @return None
        '''
        
        ## @brief Empty calibration matrix
        Betaprep = self.Beta.reshape((1,6))
        
        str_list = []
        for index in range(0,6):
            str_list.append(str(Betaprep[0,index]))
        
        ## @brief List of calibration coefficients as strings
        y = ','.join(str_list)
        
        ## @brief Object to open create and write text file
        f = open('Touch_Calibration.txt','w')
        f.write(y)
        f.close()
        
        
    def readBeta(self):
        '''!
        @brief Reads from and stores value of calibration coefficient text file
        @return None
        '''
        with open('Touch_Calibration.txt', 'r') as f:
            ## @brief Read string of calibration text file
            cal_data_string = f.readline()
            
            ## @brief Conversion of string data to list of floats
            cal_values = [float(cal_value) for cal_value in cal_data_string.strip().split(',')]
        self.Beta = numpy.array(cal_values).reshape((2,3))

        
# if __name__ == '__main__':
#     pinxm = Pin.cpu.A1
#     pinxp = Pin.cpu.A7
#     pinym = Pin.cpu.A0
#     pinyp = Pin.cpu.A6
#     test = ADC_Driver(pinxm,pinxp,pinym,pinyp)
    
    
#     q = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
#     Data = numpy.zeros(300)
    
#     a = numpy.array([1.141,0.0096,0.484,-0.0151,1.299,4.46]).reshape((2,3))
    
#     i = 0
    
#     # test.calibrate(q)
#     serport = USB_VCP()    
    
#     startTime = ticks_us()
#     print(startTime)
#     while True:
        
#         test.scanXYZ()
#         i += 1
#         if i == 100:
#             print(ticks_diff(ticks_us(),startTime)/i)
#             break
        
        
        # serport.write("\033[2K")
        # serport.write("\x0D")
        # print(test.scanXYZ())
        # serport.write(string)
        
        
        
        # if i >= 300:
            
        #     for a in Data:
        #         print(f"{a}")
        #     print("---------------------------End Data--------------------------")                
                                        
        # else:
        #     Data[i] = test.scanXYZ()[1]     
        #     i += 1
        # time.sleep(0.1)
        # stime = time.ticks_us()
        # x = test.scanX()
        # y = test.scanY()
        # z = test.scanXYZ()
        # etime = time.ticks_us()
        # print(f'{x}, {y}, {z}, Time(us):{time.ticks_diff(etime,stime)}')
        
    
    