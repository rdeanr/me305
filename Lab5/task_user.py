'''!
@file task_user.py
@brief Handles the user UI of our encoder system
@details This file interacts directly with user keyboard inputs to then send
         the request to the task_encoder.py file. It uses six states that
         correspond to different key in puts to send the program into its
         appropriate settings based on the user input. 
         
         

@author Ryan Dean
@author Zachary Hendrix
@date 2/1/2022
'''

from time import ticks_diff, ticks_add, ticks_us, ticks_ms
from pyb import USB_VCP
from shares import Share
import micropython
import array as arr
import gc
import closedloop
import os

## @brief State 0 - Initialize
#  @details State that corresponds to initializing encoder 1 and printing out
#          the initial help menu.
S0_INIT = micropython.const(0)

## @brief State 1 - Command
#  @details State that waits for a user input and transfers the user to the
#          correct state based on that input.
S1_CMD  = micropython.const(1)

## @brief State 2 - Changes mode of IMU
#  @details State that changes the mode of the IMU
S2_MODE = micropython.const(2)

## @brief State 3 - Position
#  @details State that returns position to the user
S3_POS  = micropython.const(3)

## @brief State 7 - Motor 1 Activated
#  @details State that allows user to input duty cycle for motor 1
S7_MOTOR1 = micropython.const(7)

## @brief State 8 - Motor 2 Activated
#  @details 
S8_MOTOR2 = micropython.const(8)

## @brief State 9 - Find Velocity
#  @details Calculates the velocity of motor 1
S9_VELOCITY = micropython.const(9)

## @brief State 12 - Set proportional gain
S12_Kp = micropython.const(12)

## @brief State 13 - Define set point
S13_Y = micropython.const(13)

## @brief State 14 - Enter closed loop
S14_BAL = micropython.const(14)

S15_CAL = micropython.const(15)





def taskUser(taskname, period, zFlag, pFlag, vFlag, gFlag, mFlag, MFlag, position, delta, P , T, duty, Velo, Kp, Kd,mode,opmode,cFlag,calwrite,cal_byte,calread,bFlag,duty_1,duty_2,):
    '''!@brief A generator to implement the UI task as an Finite State Machine.
        @details The task runs as a generator function and requires a task name
                 and interval to be specified. This task file receives flag
                 values altered by the user. These flags are then shared with
                 task_encoder.py where that task file can execute actions
                 defined in encoder.py. Once the desired task is completed,
                 the state is reset back to idle state (State 1).
        @param taskName The name of the task as a string.
        @param period Specifies how long the task has to perform its function
                      in microseconds. Specified as an integer.
        @param zFlag Holds True or False value to know when zeroing command was
               called by the user to zero the current position of the encoder.
        @param pFlag Holds True or False value to know when position command
                     was called by the user to know the current position of the
                     encoder.
        @param dFlag Holds True or False value to know when delta command was
                     called by the user to know the current delta (speed) of 
                     the encoder.
        @param gFlag Holds True or False value to know when data collection
                     command was called by the user to collect current time and
                     position data.
        @param position Stores current position of encoder to be read in 
                        task_user.py
        @param delta Stores the delta (speed) of the encoder to be read in
                     to task_user.py
        @param P Stores current position of encoder for data collection
        @param T Stores current time of encoder for data collection
        @param duty Stores duty value to pass into motor
        @param Dtime Represents the change in time between updates
        @param eFlag Holds True or False value to know when to enable motors
        @param mode Contains string to know whether in open or closed loop
        @param Kp Contains proportional gain value
        @param Y Contains set point value
        @return yield None            
    '''
    
    ##@brief State Variable
    # @details Stores the state we are in so the program knows which code to
    #          run
    state = S0_INIT
    
    print("+---------------------------------------------------------+")
    print("|          ME 305: Platform Controller Interface          |")
    print("+---------------------------------------------------------+")
    print("| Use the following Commands:                             |")
    print("| o or O | Select opearting mode of IMU                   |")
    print("| p or P | Print out the position of platform             |")
    print("| v or V | Print velocity of platform                     |")
    print("|   m    | Select motor 1 to set duty cycle               |")
    print("|   M    | Select motor 2 to set duty cycle               |")
    print("| b or B | Start Automatic Balancing                      |")
    print("| k or K | Enter proportional control gain                |")
    print("| d or D | Enter derivative control gain                  |")
    print("| s or S | Exit platform balance program                  |")
    print("| h or H | Return to this help page                       |")
    print("| Ctrl-C | Terminate Program                              |")
    print("+---------------------------------------------------------+")
    
    
    # A timestamp, in microseconds, indicating when the next iteration of the
    # generator must run.
    
    ##@brief Start time
    # @details Records the time in microseconds the program started running in 
    #          order to calculate the time passed since start
    start_time = ticks_us()
    
    ##@brief Next Cycle time
    # @details State that returnsthe next time in the cycle
    next_time = ticks_add(start_time, period)
    
    ##@brief Initialize the serport to read data
    # @details A (virtual) serial port object used for getting characters 
    #          cooperatively.
    serport = USB_VCP()
            
    # The finite state machine must run indefinitely.
    while True:
        # We should only call the ticks_us() function once per iteration of the 
        # of the task to "timestamp" the task iteration.
        
        ##@brief Finds current time of timer
        current_time = ticks_us()
    
        # The finite state machine only needs to run if the interval has elapsed
        # as indicated by current_time being greater than next_time.
        if ticks_diff(current_time,next_time) >= 0:
        
            # Once the task does need to run we must prepare for the next
            # iteration by adjusting the value of next_time by adding the task
            # interval. Again we can't use normal arithmetic so the ticks_add()
            # function is used.
            next_time = ticks_add(next_time, period)
            
            # State 0 code
            if state == S0_INIT:             
                # state = S1_CMD
                filename = 'IMU_cal_coeffs.txt'
                if filename in os.listdir(): #When file exists
                    with open('IMU_cal_coeffs.txt', 'r') as f:
                          if f.readline() == '':
                              print("Calibration File Corrupted...")
                              print("To calibrate, tilt the platform to every side,")
                              print("holding for a few seconds between sides. As")
                              print("the IMU calibrates the numbers of the magnetometer,")
                              print("accelerometer, and gyroscope will increase until")
                              print("a value of 3 is reached. Once calibrated, the code")
                              print("will write a new calibration file, and run the rest")
                              print("of the code.")
                              state = S15_CAL
                              cFlag.write(True)
                              i = 0
                          else:
                              state = S1_CMD # transition to state 1
                              print('Calibration file loaded')
                              calwrite.write(True)
                else:
                    print("Calibration Required...")
                    print("To calibrate, tilt the platform to every side,")
                    print("holding for a few seconds between sides. As")
                    print("the IMU calibrates the numbers of the magnetometer,")
                    print("accelerometer, and gyroscope will increase until")
                    print("a value of 3 is reached. Once calibrated, the code")
                    print("will write a new calibration file, and run the rest")
                    print("of the code.")
                    state = S15_CAL
                    cFlag.write(True)
                    i = 0    

            # State 1 code
            elif state == S1_CMD:
                
                if serport.any():

                    charIn = serport.read(1).decode()
                    
                    if charIn in {'o','O'}:
                        print("i or I = IMU")
                        print("c or C = Compass")
                        print("m or M = M4G")
                        print("d or D = NDOF_FMC_OFF")
                        print("n or N = NDOF")
                        print("Select Operating Mode:")
                        state = S2_MODE
                        zFlag.write(True)
                        
                    elif charIn in {'p', 'P'}:
                        print("Platform Position [deg]:")
                        print("(X (Heading), Y (Roll), Z (Pitch))")
                        pFlag.write(True)
                        state = S3_POS

                    elif charIn in {'h', 'H'}:
                            print("+---------------------------------------------------------+")
                            print("|          ME 305: Platform Controller Interface          |")
                            print("+---------------------------------------------------------+")
                            print("| Use the following Commands:                             |")
                            print("| o or O | Select opearting mode of IMU                   |")
                            print("| p or P | Print out the position of platform             |")
                            print("| v or V | Print velocity of platform                     |")
                            print("|   m    | Select motor 1 to set duty cycle               |")
                            print("|   M    | Select motor 2 to set duty cycle               |")
                            print("| b or B | Start Automatic Balancing                      |")
                            print("| k or K | Enter proportional control gain                |")
                            print("| d or D | Enter derivative control gain                  |")
                            print("| s or S | Exit platform balance program                  |")
                            print("| h or H | Return to this help page                       |")
                            print("| Ctrl-C | Terminate Program                              |")
                            print("+---------------------------------------------------------+")
                            
                    elif charIn == 'm':
                        buff = ''
                        state = S7_MOTOR1
                        print('Enter duty cycle for motor 1:')
                   
                    elif charIn == 'M':
                        buff = ''
                        state = S8_MOTOR2
                        print('Enter duty cycle for motor 2:')
                                   
                        
                    elif charIn in {'v', 'V'}:
                        vFlag.write(True)
                        print('Angular Velocity [degrees/s]:')
                        print("(X (Heading), Y (Roll), Z (Pitch))")
                        state = S9_VELOCITY
                
                
                    elif charIn in {'b','B'}:
                        if mode.read() == 'BAL':
                            print('Balance Mode disabled')
                            mode.write('NBAL')
                        elif mode.read() == '' or mode.read() == 'NBAL':
                            print('Balance Mode enabled')
                            mode.write('BAL')
                        Setting = 0
                    
                    
                    elif charIn in {'k','K'} and mode.read() == 'BAL':
                        print('Enter proportional gain in [%s/deg]:')
                        state = S12_Kp
                        Kp.write(0)
                        buff = ''
                        
                        
                    elif charIn in {'d', 'D'} and mode.read() == 'BAL':
                        print('Enter derivative control gain in [%/deg]')
                        state = S13_Y
                        Kd.write(0)
                        buff = ''
                        
                        
                    elif charIn in {'k','K'} and mode.read() == 'NBAL':
                        print('You must have Balance Mode enabled to use this functionality')
                        
                    
                    elif charIn in {'d', 'D'} and mode.read() == 'NBAL':
                        print('You must have Balance Mode enabled to use this functionality')
                        
                    elif charIn in {'r', 'R'} and mode.read() == 'NBAL':
                        print('Balance Mode must be enabled to use this functionality')
                                         
                    # Will need to add logic where K and Y equal something but r is also activated
                    else:
                        print(f"You typed {charIn} from state 1 at t={ticks_diff(current_time,start_time)/1e6}[s].")
                elif Kp.read() != '' and Kd.read() != '' and Setting == 0:
                    state = S14_BAL
                    CL = closedloop.ClosedLoop(Kp.read(),Kd.read())
                    bFlag.write(True)
                    
                
                elif Kp.read() != '' and Kd.read() != '' and Setting == 1:
                    state = S14_BAL
                    CL = closedloop.ClosedLoop(0, Kd.read())
                    gFlag.write(True)
                    gstart_time = ticks_ms()
                    Step = False
                    i = 0
                                               
            elif state == S2_MODE:
                if serport.any():
                    charIn = serport.read(1).decode()
                    
                    if charIn in {'i', 'I'}:
                        opmode.write(0b1000)
                        mo = 'IMU' 
                    elif charIn in {'c', 'C'}:
                        opmode.write(0b1001)
                        mo = 'COMPASS'
                    elif charIn in {'m', 'M'}:
                        opmode.write(0b1010)
                        mo = 'M4G'
                    elif charIn in {'d', 'D'}:
                        opmode.write(0b1011)
                        mo = 'NDOF_FMC_OFF'
                    elif charIn in {'n', 'N'}:
                        opmode.write(0b1100)
                        mo = 'NDOF'
                    else:
                        print("Not an available mode")
                
                elif not zFlag.read():
                    
                    print(f"{mo} mode selected")
                    state = S1_CMD
                    
            elif state == S3_POS:
                if not pFlag.read():
                    print(f"{(position.read())}")
                    
                    state = S1_CMD

            elif state == S7_MOTOR1:
                
                if serport.any():
                    
                    charIn = serport.read(1).decode()
                    
                    if charIn.isdigit():
                        buff += charIn
                    elif charIn == '-':
                        if len(buff) == 0:
                            buff = charIn
                        else:
                            print('You can only put a negative sign at the beginning')
                    elif charIn == '.':
                        if buff.count('.') == 0:
                            buff += charIn
                        else:
                            print('You can only put one decimal in the duty cycle')
                    elif charIn in {'\x7f', '\b', '\x08'}:
                        if len(buff) > 0:
                            buff = buff[0:-1]
                    elif charIn in {'\r' , '\n'}:
                        if len(buff) == 0:
                            print("Motor duty cycle remains unchanged")
                            state = S1_CMD
                        else:
                            
                            mFlag.write(True)
                            duty.write(float(buff))
                            state = S1_CMD
                        serport.write('\n')
                    serport.write(charIn)
                
                
            elif state == S8_MOTOR2:
                
                if serport.any():
        
                    charIn = serport.read(1).decode()
                    
                    if charIn.isdigit():
                        buff += charIn
                            
                    elif charIn == '-':
                        if len(buff) == 0:
                            buff = charIn
                            
                        else:
                            print('You can only put a negative sign at the beginning')
                    elif charIn == '.':
                        if buff.count('.') == 0:
                            buff += charIn
                            
                        else:
                            print('You can only put one decimal in the duty cycle')
                    elif charIn in {'\x7f', '\b', '\x08'}:
                        if len(buff) > 0:
                            buff = buff[0:-1]
                        
                                                           
                    elif charIn in {'\r' , '\n'}:
                        if len(buff) == 0:
                            print("Motor duty cycle remains unchanged")
                            state = S1_CMD
                        else:
                            MFlag.write(True)
                            duty.write(float(buff))
                            state = S1_CMD
                        serport.write('\n')
                    serport.write(charIn)    
            
            elif state == S9_VELOCITY:
                
                if not vFlag.read():
                    print(Velo.read())
                    state = S1_CMD
                                                
            elif state == S12_Kp:
                
                if serport.any():
        
                    charIn = serport.read(1).decode()
                    
                    if charIn.isdigit():
                        buff += charIn
                    elif charIn == '-':
                        if len(buff) == 0:
                            buff = charIn
                        else:
                            print('Incorrect Syntax')
                    elif charIn == '.':
                        if buff.count('.') == 0:
                            buff += charIn
                        else:
                            print('You can only put one decimal for proportional gain')
                    elif charIn in {'\x7f', '\b', '\x08'}:
                        if len(buff) > 0:
                            buff = buff[0:-1]
                                             
                    elif charIn in {'\r' , '\n'}:
                        
                        serport.write('\n')
                        
                        if len(buff) == 0:
                            print("Gain set to zero")
                            print("Kp =",Kp.read(),"[%s/rad]")
                        else:
                            Kp.write(float(buff))
                            if Kp.read() > 10:
                                Kp.write(10)
                                print("Kp =",Kp.read(),"[%s/rad]")
                            elif Kp.read() < 0:
                                Kp.write(0)
                                print("Kp =",Kp.read(),"[%s/rad]")
                            else:
                                print("Kp =",Kp.read(),"[%s/rad]")
                        buff = ''
                        state = S1_CMD
                        
                    serport.write(charIn)
            
            
            elif state == S13_Y:
                if serport.any():
        
                    charIn = serport.read(1).decode()
                    
                    if charIn.isdigit():
                        buff += charIn
                    elif charIn == '-':
                        if len(buff) == 0:
                            buff = charIn
                        else:
                            print('Incorrect Syntax')
                    elif charIn == '.':
                        if buff.count('.') == 0:
                            buff += charIn
                        else:
                            print('You can only put one decimal for proportional gain')
                    elif charIn in {'\x7f', '\b', '\x08'}:
                        if len(buff) > 0:
                            buff = buff[0:-1]
                                             
                    elif charIn in {'\r' , '\n'}:
                        
                        serport.write('\n')
                        if len(buff) == 0:
                            print("Derivative control gain set to zero")
                            print("Kd = ",Kd.read(),"[%s/deg]")
                            
                        else:
                            Kd.write(float(buff))
                            if Kd.read() < 0:
                                Kd.write(0)
                                print("Kd = ",Kd.read(),"[%s/deg]")
                            elif Kd.read() > 2:
                                Kd.write(2)
                                print("Kd = ",Kd.read(),"[%s/deg]")
                            else:
                                print("Kd = ",Kd.read(),"[%s/deg]")
                        buff = ''
                        state = S1_CMD
                        
                    serport.write(charIn)
            
            elif state == S14_BAL:
                if serport.any() and Setting == 0:
        
                    charIn = serport.read(1).decode()
                    
                    if charIn in {'s','S'}:
                        Kp.write('')
                        Kd.write('')
                        duty_1.write(0)
                        duty_2.write(0)
                        state = S1_CMD
                        print('You have returned to the command window')
                    
                    elif charIn in {'h','H'}:
                        print("+---------------------------------------------------------+")
                        print("|          ME 305: Platform Controller Interface          |")
                        print("+---------------------------------------------------------+")
                        print("| Use the following Commands:                             |")
                        print("| o or O | Select opearting mode of IMU                   |")
                        print("| p or P | Print out the position of platform             |")
                        print("| v or V | Print velocity of platform                     |")
                        print("|   m    | Select motor 1 to set duty cycle               |")
                        print("|   M    | Select motor 2 to set duty cycle               |")
                        print("| b or B | Start Automatic Balancing                      |")
                        print("| k or K | Enter proportional control gain                |")
                        print("| d or D | Enter derivative control gain                  |")
                        print("| s or S | Exit platform balance program                  |")
                        print("| h or H | Return to this help page                       |")
                        print("| Ctrl-C | Terminate Program                              |")
                        print("+---------------------------------------------------------+")    
                        
                elif Setting == 0:
                    duty_1.write(CL.run(float(position.read()[1]),-float(Velo.read()[1])))
                    if duty_1.read() > 100:
                        duty_1.write(100)
                    elif duty_1.read() < -100:
                        duty_1.write(-100)
                    duty_2.write(CL.run(-float(position.read()[2]),-float(Velo.read()[0])))
                        
            
            elif state == S15_CAL and cal_byte.read() != '':
                mag_stat = cal_byte.read()  & 0b00000011
                acc_stat = (cal_byte.read() & 0b00001100)>>2
                gyr_stat = (cal_byte.read() & 0b00110000)>>4
                sys_stat = (cal_byte.read() & 0b11000000)>>6 
                
                # print(f'Mag: {mag_stat}, Acc: {acc_stat}, gyr: {gyr_stat}, Sys: {sys_stat}')
                
                mag_stat_s = "Mag:" + str(mag_stat) + ",  "
                acc_stat_s = "Acc:" + str(acc_stat) + ",  "
                gyr_stat_s = "Gyr:" + str(gyr_stat) + ",  "
                sys_stat_s = "Sys:" + str(sys_stat)
                output = (mag_stat_s, acc_stat_s, gyr_stat_s, sys_stat_s)
                
                serport.write("\033[2K")
                serport.write("\x0D")
                string = ''.join(output)
                serport.write(string)
                
                if i >= 100 and mag_stat == 3 and acc_stat == 3 and gyr_stat == 3:
                    serport.write("\n")
                    print("System Calibrated")
                    calread.write(True)
                    cFlag.write(False)
                    state = S0_INIT
                i += 1    
                    
                
                
                    
            else:
                raise ValueError(f"Invalid state value in {taskName}: State {state} does not exist")
            
            # After a valid run of the state machine we can yield the state.
            # This yielded value could be stored in the main loop below to trace
            # the state transitions.
            yield None
        
        # If the time has not come yet to run the task we can just exit early by
        # yielding nothing.
        else:
            yield None


