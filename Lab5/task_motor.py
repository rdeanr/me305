'''!
@file task_motor.py
@brief Directly interfaces with motors
@details Task file to set duty cycle of motor 1 or motor 2. Interfaces with 
         driver.py driver file to interact with the motor. Contains logic to
         ensure there are no improper duty cycle inputs. 
         
         
                           
@author Ryan Dean
@author Zachary Hendrix
@date 2/17/2022
'''


from time import ticks_diff, ticks_add, ticks_us, ticks_ms
import motor
import pyb
from pyb import Timer, Pin, ExtInt
import time
import closedloop


def taskMotor(taskname, period, motor_1, motor_2, mFlag, MFlag, duty, duty_1, duty_2, eFlag, mode, bFlag):
    '''!
    @brief Task that is in charge of any alterations to drivers or motors
    @details This task sets the duty cycle for both mototr as well as enables 
             driver to allow said motors to operate
    @param taskname The name of the task
    @param period This task is designed to work cooperatively with other tasks.
                  The period is how long it should take for the code to circle
                  back and do this task
    @param motor_1 Object to control duty cycle of motor 1
    @param motor_2 Object ot control duty cycle of motor 2
    @param duty_1 Shared variable containing duty cycle for motor 1
    @param duty_2 Shared variable containing duty cycle for motor 2
    @param mode Flag that only allows program to run when ball balancing mode
                is enabled.
    '''
    
    # motor_1 = motor_drv.motor([1, 2], motor_drv.PWM_tim, pyb.Pin.cpu.B4, 
    #                           pyb.Pin.cpu.B5)
    # motor_2 = motor_drv.motor([3, 4], motor_drv.PWM_tim, pyb.Pin.cpu.B0, 
    #                           pyb.Pin.cpu.B1)
    
    ## @brief Start time of motor
    #  @details Assigns a variable with the time that the taskMotor
    #           function is called in microseconds.
    start_time = ticks_us()
    
    ## @brief Future moment in time
    #  @details Assigns a variable with the time that the taskMotor
    #           function is called plus the specified period the task is
    #           allowed to run.
    next_Time = ticks_add(start_time, period)
    
       
    while True:
        ## @brief Current time of Nucleo
        current_Time = ticks_us()
        
        if ticks_diff(current_Time,next_Time) >= 0:
            next_Time = ticks_add(next_Time,period)
            
            if mFlag.read():
                if duty.read() > 100:
                    duty.write(100)
                elif duty.read() < -100:
                    duty.write(-100)
                motor_1.set_duty(duty.read())
                mFlag.write(False)
            
            elif MFlag.read():
                if duty.read() > 100:
                    duty.write(100)
                elif duty.read() < -100:
                    duty.write(-100)
                motor_2.set_duty(duty.read())
                MFlag.write(False)
            
            elif mode.read() == 'BAL':
                if duty_2.read() > 100:
                    duty_2.write(100)
                elif duty_2.read() < -100:
                    duty_2.write(-100)
                
                motor_1.set_duty(duty_1.read())        
                motor_2.set_duty(duty_2.read())
            
            yield None
            
        else:
            yield None


    
    
    
