'''!
@file main.py
@brief Initializes values and manages tasks
@details This is the main file to run the tasks required to interact with the 
         encoder. It will continually call the specified task until the user
         interrupts the program by ending it with Keyboard Interrupt (Ctrl-C).
         Variables shared between tasks are initialized here and the tasks, and
         there periods are defined to run at 100Hz. Pictures of the state
         transition diagrams can be found in their respective task files. A
         picture of the task diagram can be found below.
         To view the code repository for this lab, see link below.
         
         Code repository for this lab:
         https://bitbucket.org/rdeanr/me305/src/master/Lab4/
         
         See
@ref     lab4 
         for state transition diagrams and other plots         
         
         
         
         

@author Ryan Dean
@author Zachary Hendrix
@date 2/3/2022
'''

import shares
from time import ticks_diff, ticks_add, ticks_us
from pyb import USB_VCP
import micropython
import pyb
import BNO_driver
from task_IMU import taskIMU
import motor
from task_motor import taskMotor
from task_user import taskUser
import array as arr

## @brief Signal that the button z has been pressed
#  @details Shared object so we can update its value in every file. It turns 
#           true after z is pressed, and turns false
#           after the zeroing cycle has ended.
zFlag = shares.Share(False)

## @brief Signal that the button p has been pressed
#  @details Shared object so we can update its value in every file. It turns 
#           true after z is pressed, and turns false
#           after the position has been returned.
pFlag = shares.Share(False)

## @brief Signal that the button d has been pressed
#  @details Shared object so we can update its value in every file. It turns 
#           true after z is pressed, and turns false
#          after the delta has been returned.
vFlag = shares.Share(False)

## @brief Signal that the button g has been pressed
#  @details Shared object so we can update its value in every file. It turns 
#           true after g is pressed, and turns false
#           after the data collection cycle has ended.
gFlag = shares.Share(False)

## @brief Stores position of encoder for transfer between files.
#  @details Used to communicate between task_encoder and task_user in order 
#           to pass the position across those files.
position = shares.Share(0)

## @brief Stores delta (speed) of encoder
delta = shares.Share(0)

## @brief Stores a single data point reading of encoder position
P = shares.Share(0)

## @brief Stores a single data point reading of encoder time
T = shares.Share(0)

## @brief Signal that the button m has been pressed
#  @details Shared object so we can update its value in every file. It turns 
#          true after m is pressed, and turns false
#          after the encoder 1 value has been entered.
mFlag = shares.Share(False)

## @brief Signal that the button M has been pressed
#  @details Shared object so we can update its value in every file. It turns 
#           true after M is pressed, and turns false
#          after the encoder 2 value has been entered.
MFlag = shares.Share(False)

## @brief Empty string that stores the duty cycle input
#  @details Stores the duty cycle input in order to keep code running 
#           cooperatively.
duty = shares.Share(0)

## @brief Duty Cycle for Motor 1
#  @details Holds the duty cycle for Motor 1 to transport between task_user and
#           task_motor
duty_1 = shares.Share(0)

## @brief Duty Cycle for Motor 2
#  @details Holds the duty cycle for Motor 2 to transport between task_user and
#           task_motor
duty_2 = shares.Share(0)

##@brief Stores a single data point reading the delta time between updates
Velo = shares.Share(0)

## @brief Signal that the button e has been pressed
#  @details Shared object so we can update its value in every file. It turns 
#           true after e is pressed, and turns false
#           after the driver has been enabled.
eFlag = shares.Share(False)

## @brief Signal that the button t has been pressed
#  @details Shared object so we can update its value in every file. It turns 
#           true after t is pressed, activates a testing interface that allows
#           the user to input duty cycles and recieve velocities that
#           correspond to those duty cycles
tFlag = shares.Share(False)


## @brief Signal that holds the current mode. 
#  @details Shared object so we can update its value in every file. It turns
#           true when w is pressed and indicates wheter we are in the open-loop
#           or closed loop circuit
mode = shares.Share('NBAL')

## @brief Stores the gain value 
#  @details Used to communicate between tasks in order to pass the gain across
#           files.
Kp = shares.Share('')

## @brief Stores the target velocity value 
#  @details Used to communicate between tasks in order to pass the gain across
#           files.
Kd = shares.Share('')

## @brief Shared variable that holds the IMU operating mode
opmode = shares.Share('')

## @brief Shared variable that holds the IMU operating mode
cFlag = shares.Share(False)

## @brief Shared variable that holds the IMU operating mode
calwrite = shares.Share(False)

## @brief Shared variable that holds the IMU operating mode
cal_byte = shares.Share('')

## @brief Shared variable to begin reading IMU calibration data
calread = shares.Share(False)

## @brief Shared variable to begin platofrm position and velocity
#         data collection
bFlag = shares.Share(False)

## @brief Holds task information and settings to pass into task_user.py
task1 = taskUser('taskUser', 10_000, zFlag, pFlag, vFlag, gFlag, mFlag, MFlag, position, delta,P,T, duty, Velo, Kp, Kd,mode,opmode,cFlag,calwrite,cal_byte,calread,bFlag,duty_1,duty_2)

## @brief Holds task information and settings to pass into task_encoder.py
task2 = taskIMU('taskIMU',5_000, zFlag, pFlag, vFlag, position, Velo,opmode,cFlag,calwrite,cal_byte,calread,bFlag)

## @brief The instance of the driver class
#  @details Creates a new driver instance that is used through this lab. This 
#           gets referenced to use all of the driver methods.

PWM_tim = pyb.Timer(3, freq = 20000)

motor_1 = motor.Motor([1, 2], PWM_tim, pyb.Pin.cpu.B4, 
                          pyb.Pin.cpu.B5)
motor_2 = motor.Motor([3, 4], PWM_tim, pyb.Pin.cpu.B0, 
                          pyb.Pin.cpu.B1)



## @brief Holds task information and settings to pass into task_motor.py

task3 = taskMotor('taskMotor',5_000, motor_1, motor_2, mFlag, MFlag, duty, duty_1, duty_2, eFlag, mode, bFlag)



# Creating a task list that can be iterated through. Adding more task objects 
# to the list will allow the tasks to run together cooperatively.

## @brief List of all tasks in program
#  @details This list will be used to cycle through every task in the list.
taskList = [task1, task2, task3]


# The system should run indefinitely until the user interrupts program flow
# by using a Ctrl-C to trigger a keyboard interrupt. The tasks are run by
# using the next() function on each item in the task list.
while True:
    try:
        for task in taskList:
            next(task)
    
    except KeyboardInterrupt:
        break
        
# In addition to exiting the program and informing the user any cleanup code
# should go here. An example might be un-configuring certain hardware
# peripherals like the User LED or perhaps any callback functions.
print('Program Terminating')

