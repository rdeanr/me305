'''!
@file closedloop.py
@brief Creates Closed Loop Class
@details Closed Loop Class is used to store relevent methods to calculate the 
         output duty cycle based on encoder measurements. It also allows us to 
         set the gain (Kp) and the desired velocity (Vdesired) to run the 
         calculation
         
@date (1/23/2022)
@author Ryan Dean
@author Zachary Hendrix

'''
import encoder
import pyb



encoder_1 = encoder.Encoder(pyb.Pin.board.PB6, pyb.Pin.board.PB7, 4)


class ClosedLoop:
    '''! 
    @brief ClosedLoop Class
    @details Consists of methods and variables to calculate the closed loop 
             duty cycle. Combined with main.py and task_user.py, it sets the 
             duty cycle to a value that will produce an output speed close to
             Vdesired
             
    '''
    def __init__ (self,Kp_initial,Vdesired):
        '''!
        @brief Initializes object in ClosedLoop class
        @details Takes input and creates an object that will calculate the 
                 output duty cycle
        @param Kp_initial Initial gain for closed loop
        @param Vdesired Desired speed for motor
        @return None
        '''
        self.Kp = Kp_initial
        self.Vdesired = Vdesired
        
    def run(self,Vmeasured):
        '''!
        @brief Calculates CL duty
        @details Uses Vmeasured as well as initalized parameters to 
        @param Vmeasured
        @return The closed loop duty cycle CLduty
        '''
        self.CLduty = self.Kp*(self.Vdesired - Vmeasured)
        
        return self.CLduty
        
    
    def set_Kp(self, Kp_input):
        '''!
        @brief Changes the Gain Value
        @details Changes the Gain  value for the closed loop duty cycle 
                calculation
        @param Kp_input Input gain from user for integral control
        @return None
        '''
        self.Kp = Kp_input
        

        
        
        