'''!
@file driver.py
@brief Creates DRV8847 Class
@details DRV8847 Class interfaces with motors. Enables and disables motors and
         clears faults


@date (2/13/2022)
@author Ryan Dean
@author Zachary Hendrix

'''

import motor
import pyb
from pyb import Timer, Pin
import time

class DRV8847:


    def __init__ (self, TimerVal):
        '''!
        @brief Initializes and returns a DRV8847 object.
        @details Initializes driver class with Fault pin values, Sleep pin 
                 values and the actuive timer being used for this driver. This 
                 method assigns and activates the Fault and Sleep pins.
        @param TimerVal The index that the timer is set to. The values range 
                        from 1 to 14
        '''
        
        ##@brief  Holds the pin information for the fault condition
        # @details Assigns the fault condition pin to a variable that can be 
        #          used to shut-down a driver in case of an emergency to stop 
        #          any damage to the motors
        nFault = pyb.Pin(pyb.Pin.cpu.B2)
        
        ##@brief Initializes the fault condition for the driver
        # @details Initializes the fault condition by using the nFAULT pin and 
        #          the properties of that pin to signal the code that there is 
        #          something wrong with the motor (usually high heat or a short
        #          through the wire). Once the fault pin is triggered, the 
        #          driver will shut down the motors
        self.FaultInt = pyb.ExtInt(nFault, mode=pyb.ExtInt.IRQ_FALLING, pull=pyb.Pin.PULL_NONE, callback = self.fault_cb)
        
        ##@brief Initializes the timer that the driver uses
        # @detail Sets the driver variable to pins of the ESP32 board. 
        self.PWM_tim = pyb.Timer(TimerVal, freq = 20000)
        
        ##@brief  Initializes the Sleep pin
        # @details Initializes the sleep pin that controls a pin of the board 
        #          thatenables the driver to allow the motors to spin, or  
        #          disable the driver to keep the motors from spinning if there
        #          is a fault.
        self.nSLEEP = Pin(Pin.cpu.A15, mode=Pin.OUT_PP)
        
        
        
        
    
    def enable (self):
        '''!
        @brief Brings the DRV8847 out of sleep mode.
        @details Activates the SLEEP pin and allows the motors to power up. 
        '''
        self.FaultInt.disable()
        self.nSLEEP.high()
        time.sleep_us(50)
        self.FaultInt.enable()
        
    
    def disable (self):
        '''!@brief Puts the DRV8847 in sleep mode.
            @details Deactivates the SLEEP pin and does not allow the motors to
                     spin
        '''
        self.nSLEEP.low()
        
    
    
    def fault_cb (self, IRQ_src):
        '''!@brief Callback function to run on fault condition.
            @details Deactivates the driver once a fault is detected. It then 
                     prints to the user that the driver has shut down due to a 
                     fault
            @param IRQ_src The source of the interrupt request.
        '''
        
        self.disable()    
        print('Motors disabled: Press c or C to clear fault and re-enable motors')
        
     
    
    def motor(self,channel,PWM_tim,IN1_pin,IN2_pin):
        '''!@brief Creates a DC motor object connected to the DRV8847.
            @param channel Contains channel number for specific motor used
            @param PWM_tim Contains the the timer value for proper PWM
            @param IN1_pin Object to interface with pin that determines rotation
                   direction
            @param IN2_pin Object to interface with pin that determines rotation
                   direction
            @return An object of class Motor
        '''
        
        return motor.Motor(channel,PWM_tim,IN1_pin,IN2_pin)
    
   
            
         
            