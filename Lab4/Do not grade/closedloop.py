import encoder
import pyb



encoder_1 = encoder.Encoder(pyb.Pin.board.PB6, pyb.Pin.board.PB7, 4)


class ClosedLoop:
    def __init__ (self,Kp_initial,Vdesired):
        self.Kp = Kp_initial
        self.Vdesired = Vdesired
        
    def run(self,Vmeasured):
        
        self.CLduty = self.Kp*(self.Vdesired - Vmeasured)
        
        return self.CLduty
        
    
    def set_Kp(self, Kp_input):
        self.Kp = Kp_input
        

        
        
        