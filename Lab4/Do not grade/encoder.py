'''!
@file encoder.py
@brief Creates Encoder Class
@details Encoder Class takes inputs from an ESP32 Nucleo and counts the number 
         of ticks a motor rotates past. It uses this to calculate the change in
         posistion and adds it to the overall position with a correction for 
         over/underflow.
         
@date (1/23/2022)
@author Ryan Dean
@author Zachary Hendrix

'''

import pyb
import time
from shares import Share
from time import ticks_diff, ticks_add, ticks_us, ticks_ms

class Encoder:
    '''! 
    @brief Encoder Class
    @details Consists of methods and variables to calculate rotational position
             and delta (speed) of a rotating object. Combined with main.py, 
             it outputs the position of the object from a selected zero point.
    '''
    
    def __init__(self,pin_one,pin_two,TimerNum):
        '''!
        @brief Initializes object in Encoder class
        @details Takes input and creates an object with configured settings to
                 appropriately interact with ESP32 and motor
        @param pin_one
        @param pin_two
        @param TimerNum
        @return Encoder object with EncoderName, Text relaying an encoder has 
                been created
        '''
        
        ## @brief pyb.Timer class setting prescaler to 0 and specifying the 
        #         auto-reload value ot 65535 encoder ticks
        #  @details This sets whatever timer number is selected to count every
        #           tick outputted from the encoder (prescale = 0) and
        #           the auto-reload value (period) to 65535. This value is 
        #           inherent to the STM32 Nucleo used for this program
        self.timer = pyb.Timer(TimerNum, prescaler=0, period = 65535)
        
        ## @brief pyb.Timer channel 1 object created.
        #  @details Configures channel to Encoder mode. The counter will now
        #           change when ch1 changes.
        self.ch1 = self.timer.channel(1, pyb.Timer.ENC_AB,pin=pin_one)
        
        ## @brief pyb.Timer channel 2 object created.
        #  @details Configures channel to Encoder mode. The counter will now
        #           change when ch2 changes.
        self.ch2 = self.timer.channel(2, pyb.Timer.ENC_AB,pin=pin_two)
        
        ## @brief The position at the beginning of the iteration.
        #  @details Position that is used to calculate delta of the two
        #           positions by storing a tick count from a previous update.
        self.old = 0
        
        ## @brief The position after the time has ended
        #  @details Position that is used to calculate delta of the two 
        #           positions. This holds the most recent tick reading of the
        #           Nucleo. Once position is calculated, self.old is assigned
        #           with self.new, allowing the Nucleo to continually update
        #           position with each iteration.
        self.new = 0
        
        ## @brief Position of the encoder
        #  @details Determined by adding each delta to it to output the overall
        #           distance the encoder has traveled in the time since the
        #           encoder was initialized
        self.position = 0
        self.deltatimeold = ticks_us()
        
    def zero(self):
        '''!
        @brief Sets the current position to zero
        @details Primes update() function by setting 'A' list index 0 to '-' 
                 and index  1 to zero. Resets encoder position reading to zero. 
        @return Text relaying that the current location is zero
        '''
        
        ## @brief Stores counter values
        #  @details Initially defined with a string as the first element, the 
        #           this list is used to store counter values each time the
        #           class is called. This list is updated with each call,
        #           where the difference of its 0th and 1st index is used for
        #           encoder postion calculations.
                
        ## @brief Position of encoder
        self.position = 0
            
    
    def update(self):
        '''!
        @brief Updates position
        @details Works with 2^16 timers, if first run, the first 'if' loop runs
                 , initializing the run. At the end of the first run, the 
                 position value is stored as the 'past' encoder value, and the
                 '-' in the 'A' list is replaced. The second run records the
                 new count of the encoder but replaces index 1 of 'A'. The
                 difference between these indeces is then calculated. Logic is
                 implemented to account for overflow/underflow and the position
                 is updated.
        @return None
        '''
        
        ## @brief Auto-reload value of STM 32 Nucleo MPU
        AR = 65535
        
        ## @brief Difference between encoder update() iterations
        #  @details The difference between update iterations are what is added
        #           over time to compute position. Once an overflow/underflow
        #           condition is reached, the delta is corrected based on the
        #           auto reload value that the Nucleo would normally reset at.
        self.delta = 0
        
        self.new = self.timer.counter()
        self.delta = self.new-self.old
        self.deltatimenew = ticks_us()
        
        if self.delta > (AR + 1)/2:
            self.delta -= AR + 1
            self.position += self.delta
        elif self.delta < (-AR + 1)/2:
            self.delta += AR + 1 
            self.position += self.delta
        else:
            self.position += self.delta
        self.old = self.new
        
        self.deltatime = ticks_diff(self.deltatimenew, self.deltatimeold)/1e6
        self.deltatimeold = self.deltatimenew
        
    def get_position(self):
        '''!
        @brief Returns position
        @details Returns current position from an encoder
        @return position
        '''
        return self.position

        
    def get_deltatime(self):
        '''!
        @brief Returns delta time
        @details Returns delta time used to calculate velocity
        @return deltatime
        '''
        return self.deltatime
    
    def get_delta(self):
        '''!
        @brief Returns delta
        @details Return current change in position from an encoder
        @return delta
        '''
        return self.delta