'''!
@file task_user.py
@brief Handles the user UI of our encoder system
@details This file interacts directly with user keyboard inputs to then send
         the request to the task_encoder.py file. It uses six states that
         correspond to different key in puts to send the program into its
         appropriate settings based on the user input. 
         
         

@author Ryan Dean
@author Zachary Hendrix
@date 2/1/2022
'''

from time import ticks_diff, ticks_add, ticks_us, ticks_ms
from pyb import USB_VCP
from shares import Share
import micropython
from task_encoder import taskEncoder
import encoder
import array as arr
import gc
import closedloop

## @brief State 0 - Initialize
#  @details State that corresponds to initializing encoder 1 and printing out
#          the initial help menu.
S0_INIT = micropython.const(0)

## @brief State 1 - Command
#  @details State that waits for a user input and transfers the user to the
#          correct state based on that input.
S1_CMD  = micropython.const(1)

## @brief State 2 - Zero Encoder
#  @details State that zeros out the encoder
S2_ZERO = micropython.const(2)

## @brief State 3 - Position
#  @details State that returns position to the user
S3_POS  = micropython.const(3)

## @brief State 4 - Delta
#  @details State that returns the delta(speed) of the encoder to the user
S4_DEL  = micropython.const(4)

## @brief State 5 - Data Collection
#  @details State that collects 30 seconds of data and outputs it to the user
S5_DATA = micropython.const(5)

## @brief State 7 - Motor 1 Activated
#  @details State that allows user to input duty cycle for motor 1
S7_MOTOR1 = micropython.const(7)

## @brief State 8 - Motor 2 Activated
#  @details 
S8_MOTOR2 = micropython.const(8)

## @brief State 9 - Find Velocity
#  @details Calculates the velocity of motor 1
S9_VELOCITY = micropython.const(9)

## @brief State 10 - Enables motors  
# 
S10_ENABLE = micropython.const(10)

## @brief State 11 - Start testing interface
#  @details State for testing interface for users to input duty and velocity
S11_TEST = micropython.const(11)

S12_Kp = micropython.const(12)

S13_Y = micropython.const(13)

S14_CL = micropython.const(14)



def taskUser(taskname, period, zFlag, pFlag, dFlag, gFlag, mFlag, MFlag, position, delta, P , T, duty, Dtime, eFlag, mode, Kp, Y):
    '''!@brief A generator to implement the UI task as an Finite State Machine.
        @details The task runs as a generator function and requires a task name
                 and interval to be specified. This task file receives flag
                 values altered by the user. These flags are then shared with
                 task_encoder.py where that task file can execute actions
                 defined in encoder.py. Once the desired task is completed,
                 the state is reset back to idle state (State 1).
        @param taskName The name of the task as a string.
        @param period Specifies how long the task has to perform its function
                      in microseconds. Specified as an integer.
        @param zFlag Holds True or False value to know when zeroing command was
               called by the user to zero the current position of the encoder.
        @param pFlag Holds True or False value to know when position command
                     was called by the user to know the current position of the
                     encoder.
        @param dFlag Holds True or False value to know when delta command was
                     called by the user to know the current delta (speed) of 
                     the encoder.
        @param gFlag Holds True or False value to know when data collection
                     command was called by the user to collect current time and
                     position data.
        @param position Stores current position of encoder to be read in 
                        task_user.py
        @param delta Stores the delta (speed) of the encoder to be read in
                     to task_user.py
        @param P Stores current position of encoder for data collection
        @param T Stores current time of encoder for data collection
        @return yield None            
    '''
    
    ##@brief State Variable
    # @details Stores the state we are in so the program knows which code to
    #          run
    state = S0_INIT
    
    print("+---------------------------------------------------------+")
    print("|          ME 305: Encoder Testing Interface              |")
    print("+---------------------------------------------------------+")
    print("| Use the following Commands:                             |")
    print("| z or Z | Zero the position of encoder 1                 |")
    print("| p or P | Print out the position of encoder 1            |")
    print("| p or P | Print out the position of encoder 1            |")
    print("| d or D | Print the delta (speed) of encoder 1           |")
    print("| e or E | Enable motor 1 and motor 2    ************     |")
    print("|   m    | Select motor 1 to set duty cycle               |")
    print("|   M    | Select motor 2 to set duty cycle               |")
    print("| v or V | Print velocity of encoder 1                    |")
    print("| c or C | Clear a fault condition triggered by DRV8847   |")
    print("| g or G | Collect 30 seconds of data from motor 1        |")
    print("|        | Output:[Time[s],Position[rad],Velocity[rad/s]  |")
    print("| t or T | Start testing for duty and velocity table      |")
    print("| s or S | End data collection early                      |")
    print("| h or H | Return to this help page                       |")
    print("| Ctrl-C | Terminate Program                              |")
    print("+---------------------------------------------------------+")
    
    # A timestamp, in microseconds, indicating when the next iteration of the
    # generator must run.
    
    ##@brief Start time
    # @details Records the time in microseconds the program started running in 
    #          order to calculate the time passed since start
    start_time = ticks_us()
    
    ##@brief Next Cycle time
    # @details State that returnsthe next time in the cycle
    next_time = ticks_add(start_time, period)
    
    ##@brief Initialize the serport to read data
    # @details A (virtual) serial port object used for getting characters 
    #          cooperatively.
    serport = USB_VCP()
    
    ##@brief Time array
    # @details Array used to store variables of time
    #          collected over 30 seconds (3000 indicies)
    DataP = arr.array('f', 1001*[0])


    ##@brief Position array
    # @details Array used to store variables of position
    #          collected over 30 seconds (3000 indicies)
    DataT = arr.array('f', 1001*[0])


    DataV = arr.array('f', 1001*[0])


    
    DataDuty = arr.array('f',100*[0])
    
    DataVelo = arr.array('f',100*[0])
    
    DataAvgV = arr.array('f',40*[0])
    
       
            
    # The finite state machine must run indefinitely.
    while True:
        # We should only call the ticks_us() function once per iteration of the 
        # of the task to "timestamp" the task iteration. This value can be used 
        # internally within the states if timestamps are require for data
        # collection.
        
        ##@brief Finds current time of timer
        current_time = ticks_us()
    
        # The finite state machine only needs to run if the interval has elapsed
        # as indicated by current_time being greater than next_time. We can't
        # do direct comparison or arithmetic of ticks_us() values because of the
        # internal rollover/overflow of the ticks value. Therefore we have to
        # use ticks_diff() to find the difference in ticks value which then can 
        # be compared to zero to check which is greater.
        if ticks_diff(current_time,next_time) >= 0:
        
            # Once the task does need to run we must prepare for the next
            # iteration by adjusting the value of next_time by adding the task
            # interval. Again we can't use normal arithmetic so the ticks_add()
            # function is used.
            next_time = ticks_add(next_time, period)
            
            # State 0 code
            if state == S0_INIT:             
                state = S1_CMD # transition to state 1
            
                
            # State 1 code
            elif state == S1_CMD:
                
                if serport.any():
                # Read one character and decode it into a string
                    ##@brief Input Character
                    # @details Reads one character and decodes it into a string
                    #          . Then it stores the input character.
                    charIn = serport.read(1).decode()
                    
                    if charIn in {'z','Z'}:
                        print("Zeroing Encoder")
                        zFlag.write(True)
                        state = S2_ZERO
                    elif charIn in {'p', 'P'}:
                        print("Encoder Position [rad]:")
                        pFlag.write(True)
                        state = S3_POS
                    elif charIn in {'d', 'D'}:
                        print("Delta Position")
                        dFlag.write(True)
                        state = S4_DEL
                    elif charIn in {'g', 'G'}:
                        print("-------------------Collecting Data---------------------")
                        gFlag.write(True)
                        dFlag.write(True)
                                         
                        ##@brief Initial time of data collection
                        # @details Stores the inital time of the data
                        #          collection sequence to calculate relative
                        #          time from the start of the sequence
                        gstart_time = ticks_ms()
                        state = S5_DATA
                       
                        ##@brief Data Collection index
                        # @details Used to put data in the right place in the
                        #          DataP and DataT array
                        i = 0
                    elif charIn in {'h', 'H'}:
                            print("+---------------------------------------------------------+")
                            print("|          ME 305: Encoder Testing Interface              |")
                            print("+---------------------------------------------------------+")
                            print("| Use the following Commands:                             |")
                            print("| z or Z | Zero the position of encoder 1                 |")
                            print("| p or P | Print out the position of encoder 1            |")
                            print("| p or P | Print out the position of encoder 1            |")
                            print("| d or D | Print the delta (speed) of encoder 1           |")
                            print("| e or E | Enable motor 1 and motor 2    ************     |")
                            print("|   m    | Select motor 1 to set duty cycle               |")
                            print("|   M    | Select motor 2 to set duty cycle               |")
                            print("| v or V | Print velocity of encoder 1                    |")
                            print("| c or C | Clear a fault condition triggered by DRV8847   |")
                            print("| g or G | Collect 30 seconds of data from motor 1        |")
                            print("|        | Output:[Time[s],Position[rad],Velocity[rad/s]  |")
                            print("| t or T | Start testing for duty and velocity table      |")
                            print("| s or S | End data collection early                      |")
                            print("| h or H | Return to this help page                       |")
                            print("| Ctrl-C | Terminate Program                              |")
                            print("+---------------------------------------------------------+")
                    elif charIn == 'm':
                        buff = ''
                        state = S7_MOTOR1
                        print('Enter duty cycle for motor 1:')
                    elif charIn == 'M':
                        buff = ''
                        state = S8_MOTOR2
                        print('Enter duty cycle for motor 2:')
                    elif charIn in {'v', 'V'}:
                        dFlag.write(True)
                        print('Velocity [rad/s]:')
                        state = S9_VELOCITY
                    elif charIn in {'e','E','c','C'}:
                        eFlag.write(True)
                        print('Motors enabled')
                        state = S10_ENABLE
                        
                    elif charIn in {'t','T'}:
                        mFlag.write(True)
                        duty.write(0)
                        gFlag.write(True)
                        buff = ''
                        print("Enter a duty cycle for motor 1, press s or S to exit interface")
                        q = 1
                        n = 0
                        k = 0
                        state = S11_TEST
                    
                    elif charIn in {'w','W'}:
                        eFlag.write(True)
                        if mode.read() == 'CL':
                            print('Open Loop Control Enabled')
                            mode.write('OL')
                        elif mode.read() == '' or mode.read() == 'OL':
                            print('Closed Loop Control Enabled')
                            mode.write('CL')
                        Setting = 0
                    
                    elif charIn in {'k','K'} and mode.read() == 'CL':
                        print('Enter proportional gain in [%s/rad]:')
                        state = S12_Kp
                        buff = ''
                        
                    elif charIn in {'y', 'Y'} and mode.read() == 'CL':
                        print('Enter set point for closed-loop control [rad/s]')
                        state = S13_Y
                        buff = ''
                        
                    elif charIn in {'k','K'} and mode.read() == 'OL':
                        print('You must have closed-loop control enabled to use this functionality')
                        
                    
                    elif charIn in {'y', 'Y'} and mode.read() == 'OL':
                        print('You must have closed-loop control enabled to use this functionality')
                    
                    elif charIn in {'r', 'R'} and mode.read() == 'CL':
                        Kp.write('')
                        Y.write('')
                        Setting = 1
                        print('Step Response on Motor 1 Selected')
                        print('Enter proportional gain and set point')
                        
                    elif charIn in {'r', 'R'} and mode.read() == 'OL':
                        print('Closed-loop control must be enabled to use this functionality')
                                         
                    # Will need to add logic where K and Y equal something but r is also activated
                    else:
                        print(f"You typed {charIn} from state 1 at t={ticks_diff(current_time,start_time)/1e6}[s].")
                elif Kp.read() != '' and Y.read() != '':
                    state = S14_CL
                    CL = closedloop.ClosedLoop(Kp.read(),Y.read())
                    gFlag.write(True)
                                
            elif state == S2_ZERO:
                if not zFlag.read():
                    print("Zeroing Completed")
                    state = S1_CMD
            elif state == S3_POS:
                if not pFlag.read():
                    print(f"{(position.read()/4000)*2*3.1415:.2f}")
                    state = S1_CMD
            elif state == S4_DEL:
                if not dFlag.read():
                    print(f"{(delta.read()/4000)*2*3.1415:.2f}")
                    state = S1_CMD
            elif state == S5_DATA:
                
                if serport.any():
                    charIn = serport.read(1).decode()
                    
                    if charIn in {'s', 'S'}:
                        gFlag.write(False)
                        
                        for (a,b,c) in zip(DataT[:i],DataP[:i],DataV[:i]):
                            print(f"{(a - gstart_time)/1000:.2f}, {(b/4000)*2*3.1415:.2f}, {c}")
                        print("---------------------------End Data--------------------------")
                        state = S1_CMD                        
                
                elif i >= 1001:
                    gFlag.write(False)
                    dFlag.write(False)
                    
                    for (a,b,c) in zip(DataT,DataP,DataV):
                        print(f"{(a - gstart_time)/1000:.2f}, {(b/4000)*2*3.1415:.2f}, {c}")
                    print("---------------------------End Data--------------------------")                
                    state = S1_CMD
                                    
                else:
                    DataT[i] = T.read()
                    DataP[i] = P.read()
                    DataV[i] = 2*3.1415*(delta.read()/Dtime.read())/4000
                    i += 1
                    
                    
            elif state == S7_MOTOR1:
                
                if serport.any():
        
                    charIn = serport.read(1).decode()
                    
                    if charIn.isdigit():
                        buff += charIn
                    elif charIn == '-':
                        if len(buff) == 0:
                            buff = charIn
                        else:
                            print('You can only put a negative sign at the beginning')
                    elif charIn == '.':
                        if buff.count('.') == 0:
                            buff += charIn
                        else:
                            print('You can only put one decimal in the duty cycle')
                    elif charIn in {'\x7f', '\b', '\x08'}:
                        if len(buff) > 0:
                            buff = buff[0:-1]
                    elif charIn in {'\r' , '\n'}:
                        if len(buff) == 0:
                            print("Motor duty cycle remains unchanged")
                            state = S1_CMD
                        else:
                            mFlag.write(True)
                            duty.write(float(buff))
                            state = S1_CMD
                        serport.write('\n')
                    serport.write(charIn)
                
                
            elif state == S8_MOTOR2:
                
                if serport.any():
        
                    charIn = serport.read(1).decode()
                    
                    if charIn.isdigit():
                        buff += charIn
                            
                    elif charIn == '-':
                        if len(buff) == 0:
                            buff = charIn
                            
                        else:
                            print('You can only put a negative sign at the beginning')
                    elif charIn == '.':
                        if buff.count('.') == 0:
                            buff += charIn
                            
                        else:
                            print('You can only put one decimal in the duty cycle')
                    elif charIn in {'\x7f', '\b', '\x08'}:
                        if len(buff) > 0:
                            buff = buff[0:-1]
                        
                                                           
                    elif charIn in {'\r' , '\n'}:
                        if len(buff) == 0:
                            print("Motor duty cycle remains unchanged")
                            state = S1_CMD
                        else:
                            mFlag.write(True)
                            duty.write(float(buff))
                            state = S1_CMD
                        serport.write('\n')
                    serport.write(charIn)    
            
            elif state == S9_VELOCITY:
                if not dFlag.read():
                    velocity = 2*3.1415*(delta.read()/Dtime.read())/4000
                    if velocity == 0.0:
                        velocity = abs(velocity)
                    print(velocity)
                    state = S1_CMD
            elif state == S10_ENABLE:
                if not eFlag.read():
                    state = S1_CMD
            
            elif state == S11_TEST:
                if q == 1:
                
                    if serport.any():
            
                        charIn = serport.read(1).decode()
                        
                        if charIn.isdigit():
                            buff += charIn
                        elif charIn == '-':
                            if len(buff) == 0:
                                buff = charIn
                            else:
                                print('You can only put a negative sign at the beginning')
                        elif charIn == '.':
                            if buff.count('.') == 0:
                                buff += charIn
                            else:
                                print('You can only put one decimal in the duty cycle')
                        elif charIn in {'\x7f', '\b', '\x08'}:
                            if len(buff) > 0:
                                buff = buff[0:-1]
                                                   
                        elif charIn in {'s','S'}:
                            
                            gFlag.write(False)
                            state = S1_CMD
                            buff = '0'
                            charIn = ''
                            mFlag.write(True)
                            duty.write(float(buff))
                            for (a,b) in zip(DataDuty[:k],DataVelo[:k]):
                                print(f"{a}, {b:.2f}")
                        
                        elif charIn in {'c', 'C'}:
                            eFlag.write(True)
                            state = S1_CMD
                            print('Motors enabled, you have returned to the command window')
                            charIn = ''
                        elif charIn in {'\r' , '\n'}:
                            mFlag.write(True)
                            duty.write(float(buff))
                            serport.write('\n')
                            buff = ''
                            if duty.read() > 100:
                                duty.write(100)
                            elif duty.read() < -100:
                                duty.write(-100)
                            DataDuty[k] = duty.read()
                            q = 0 
                            
                        serport.write(charIn)
                        
                elif n < 40 and q == 0:
                    DataAvgV[n] = 2*3.1415*(delta.read()/Dtime.read())/4000
                    n += 1
                
                elif n == 40:
                    DataVelo[k] = sum(DataAvgV)/len(DataAvgV)
                    k += 1
                    q = 1
                    n = 0
                    
                    # elif charIn in {'s', 'S'}:
                        # buff == charIn
                                                
            elif state == S12_Kp:
                
                if serport.any():
        
                    charIn = serport.read(1).decode()
                    
                    if charIn.isdigit():
                        buff += charIn
                    elif charIn == '-':
                        if len(buff) == 0:
                            buff = charIn
                        else:
                            print('Incorrect Syntax')
                    elif charIn == '.':
                        if buff.count('.') == 0:
                            buff += charIn
                        else:
                            print('You can only put one decimal for proportional gain')
                    elif charIn in {'\x7f', '\b', '\x08'}:
                        if len(buff) > 0:
                            buff = buff[0:-1]
                                             
                    elif charIn in {'\r' , '\n'}:
                        Kp.write(float(buff))
                        serport.write('\n')
                        buff = ''
                        if Kp.read() > 5:
                            Kp.write(5)
                        elif Kp.read() < 0:
                            Kp.write(0)
                        state = S1_CMD
                        
                    serport.write(charIn)
            
            
            elif state == S13_Y:
                if serport.any():
        
                    charIn = serport.read(1).decode()
                    
                    if charIn.isdigit():
                        buff += charIn
                    elif charIn == '-':
                        if len(buff) == 0:
                            buff = charIn
                        else:
                            print('Incorrect Syntax')
                    elif charIn == '.':
                        if buff.count('.') == 0:
                            buff += charIn
                        else:
                            print('You can only put one decimal for proportional gain')
                    elif charIn in {'\x7f', '\b', '\x08'}:
                        if len(buff) > 0:
                            buff = buff[0:-1]
                                             
                    elif charIn in {'\r' , '\n'}:
                        Y.write(float(buff))
                        serport.write('\n')
                        buff = ''
                        if Y.read() < -100:
                            Y.write(-100)
                        elif Y.read() > 100:
                            Y.write(100)
                        state = S1_CMD
                        
                    serport.write(charIn)
            
            elif state == S14_CL:
                if Setting == 0:
                    duty.write(CL.run(2*3.1415*(delta.read()/Dtime.read())/4000))
                    print(2*3.1415*(delta.read()/Dtime.read())/4000)
                    print(duty.read())
                    state = S1_CMD
                    
                elif Setting == 1:    
                    # Data Collection step response
                    
                    pass
            else:
                raise ValueError(f"Invalid state value in {taskName}: State {state} does not exist")
            
            # After a valid run of the state machine we can yield the state.
            # This yielded value could be stored in the main loop below to trace
            # the state transitions.
            yield None
        
        # If the time has not come yet to run the task we can just exit early by
        # yielding nothing.
        else:
            yield None


