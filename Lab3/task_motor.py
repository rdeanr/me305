'''!
@file task_motor.py
@brief Directly interfaces with motors
@details Task file to set duty cycle of motor 1 or motor 2. Interfaces with 
         driver.py driver file to interact with the motor. Contains logic to
         ensure there are no improper duty cycle inputs. 
         
         
                           
@author Ryan Dean
@author Zachary Hendrix
@date 2/17/2022
'''


from time import ticks_diff, ticks_add, ticks_us, ticks_ms
import motor
import pyb
from pyb import Timer, Pin, ExtInt
import time
import driver



def taskMotor(taskname, period, motor_drv, mFlag, MFlag, duty, eFlag):
    '''!
    @brief Task that is in charge of any alterations to drivers or motors
    @details This task sets the duty cycle for both mototr as well as enables 
             driver to allow said motors to operate
    @param taskname The name of the task
    @param period This task is designed to work cooperatively with other tasks.
                  The period is how long it should take for the code to circle
                  back and do this task
    @param motor_drv The motor driver name, used to refernce the motors being 
                     used in this task
    @param mFlag A flag that is active when the m button was pressed in task
                 user.
    @param MFlag A flag that is active when the M button was pressed in task
                 user. Holds True or False value dependinf the input from the 
                 user in task_user.py. initiates the input duty cycle to motor2
    @param duty A shared variable that hold the duty cycle value for either 
                motor in the driver
    @param eFlag A flag that is active when the e button was pressed in task
                 user. Used to tell this task to enable the driver.
    '''
    
    motor_1 = motor_drv.motor([1, 2], motor_drv.PWM_tim, pyb.Pin.cpu.B4, 
                              pyb.Pin.cpu.B5)
    motor_2 = motor_drv.motor([3, 4], motor_drv.PWM_tim, pyb.Pin.cpu.B0, 
                              pyb.Pin.cpu.B1)
    
    ## @brief Start time of encoder
    #  @details Assigns a variable with the time that the taskEncoder
    #           function is called in microseconds.
    start_time = ticks_us()
    
    ## @brief Future moment in time
    #  @details Assigns a variable with the time that the taskEncoder
    #           function is called plus the specified period the task is
    #           allowed to run.
    next_Time = ticks_add(start_time, period)
    
       
    while True:
        ## @brief Current time of Nucleo
        current_Time = ticks_us()
        
        if ticks_diff(current_Time,next_Time) >= 0:
            next_Time = ticks_add(next_Time,period)
            
            if mFlag.read():
                if duty.read() > 100:
                    duty.write(100)
                elif duty.read() < -100:
                    duty.write(-100)
                motor_1.set_duty(duty.read())
                mFlag.write(False)
            
            elif MFlag.read():
                if duty.read() > 100:
                    duty.write(100)
                elif duty.read() < -100:
                    duty.write(-100)
                motor_2.set_duty(duty.read())
                MFlag.write(False)
            
            elif eFlag.read():
                motor_drv.enable()
                # Enabled.write(True)
                motor_1.set_duty(0)
                motor_2.set_duty(0)
                eFlag.write(False)
            
            yield None
            
        else:
            yield None


    
    
    
