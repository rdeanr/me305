'''!
@file task_encoder.py
@brief Directly interfaces with encoder
@details Called at a frequency of 100Hz, this task file uses the class Encoder 
         within the encoder.py drive file to interface with the encoder.
         Continually updating the position of the encoder, this task file uses
         flag values altered by the user in task_user.py to execute actions
         defined in encoder.py. Once the action is completed, these flags are
         reset.
         
         
                  
@author Ryan Dean
@author Zachary Hendrix
@date 2/1/2022
'''

import encoder
import pyb
from time import ticks_diff, ticks_add, ticks_us, ticks_ms
from pyb import USB_VCP
from shares import Share
import array as arr



def taskEncoder(taskname, period, zFlag, pFlag, dFlag, gFlag, position, delta,P,T,Dtime):
    '''!
    @brief Generator function to inteface with encoder
    @details Generator function receives values of certain flags that
             correspond to a desired action from the user. This then triggers
             the function to interact with the encoder.py drive file, 
             performing the necessary action or storing the value for the user
             to then see with their interface. 
    @param taskname Name of the task being performed.
    @param period Specifies how long the task has to perform its function.
                  received as microseconds. Specified as an integer.
    @param zFlag Holds True or False value to know when zeroing command was
           called by the user to zero the current position of the encoder.
    @param pFlag Holds True or False value to know when position command was
           called by the user to know the current position of the encoder.
    @param dFlag Holds True or False value to know when delta command was
           called by the user to know the current delta (speed) of the encoder.
    @param gFlag Holds True or False value to know when data collection command
                 was called by the user to collect current time and position 
                 data.
    @param position Stores current position of encoder to be transferred to 
                    task_user.py
    @param delta Stores the delta (speed) of the encoder to be transferred to
                 task_user.py
    @param P Stores current position of encoder for data collection
    @param T Stores current time of encoder for data collection
    @param Dtime Contains value for the change in time between updates
    @return yields None
    '''
    
    
    ## @brief Start time of encoder
    #  @details Assigns a variable with the time that the taskEncoder
    #           function is called in microseconds.
    start_time = ticks_us()
    
    ## @brief Future moment in time
    #  @details Assigns a variable with the time that the taskEncoder
    #           function is called plus the specified period the task is
    #           allowed to run.
    next_Time = ticks_add(start_time, period)
    
    ## @brief Creates an object using the Encoder class of encoder.py
    #  @details Creates a object using the Encoder class of encoder.py and
    #           specifies the appropriate pins to receive data and the 
    #           appropriate Timer number to receive time data.
    encoder_1 = encoder.Encoder(pyb.Pin.board.PB6, pyb.Pin.board.PB7, 4)  
    
    
    while True:
        ## @brief Current time of Nucleo
        current_Time = ticks_us()
        
        encoder_1.update()
        
        if ticks_diff(current_Time,next_Time) >= 0:
            next_Time = ticks_add(next_Time,period)
                       
            if zFlag.read():
                encoder_1.zero()
                zFlag.write(False)
            
            elif pFlag.read():
                position.write(encoder_1.get_position())
                pFlag.write(False)
            
            elif dFlag.read():
                delta.write(encoder_1.get_delta())
                dFlag.write(False)
                Dtime.write(encoder_1.get_deltatime())
                
            elif gFlag.read():
                # encoder_1.update()
                T.write(ticks_ms())
                P.write(encoder_1.get_position())             
                delta.write(encoder_1.get_delta())
                Dtime.write(encoder_1.get_deltatime())


            
            yield None
            
        else:
            yield None
    

